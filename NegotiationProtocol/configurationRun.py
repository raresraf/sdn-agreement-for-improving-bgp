import networkx as nx

from NegotiationProtocol import controllers, randomizers
from NegotiationProtocol.CP import ConfigSender, readConfigSender
from NegotiationProtocol.ISP import ConfigReceiver, readConfigReceiver
from NegotiationProtocol.bandwidthOptimizers import configurationRunBandwidth, advancedNegotiation
from NegotiationProtocol.controllers import createDeepCopyConfigSender, createDeepCopyConfigReceiver, createGraph
from NegotiationProtocol.latencyOptimizers import configurationRunShortestLatencyPaths, optimizerCommunicationAdvanced
from NegotiationProtocol.plot import plotResults, plotResultsBandwidthOptimizers, plotResultsLatencyOptimizers, \
    plotAggregatedFlowsBandwidthNegotiation, plotCDF

NO_TOPOLOGIES_EXAMINED = 40

topology_name = "_Rocketfuel_rf1239"
log_file = "results/log" + topology_name + ".txt"
results_file = "results/routingResults" + topology_name + ".txt"

all_topologies = ["_Rocketfuel_rf1221", "_Rocketfuel_rf1239", "_Rocketfuel_rf1755", "_Rocketfuel_rf3257",
                  "_Rocketfuel_rf3967", "_Rocketfuel_rf6461"]


def comparisonBandDelBandNeg(topology, configSender, configReceiver):
    aggregated_results_band_neg_links_modified = []
    for i in range(0, NO_TOPOLOGIES_EXAMINED):
        tempTopologyLinks = topology.copy()
        tempConfigSenderLinks = createDeepCopyConfigSender(configSender)
        tempConfigReceiverLinks = createDeepCopyConfigReceiver(configReceiver)

        neg_opt_bandwidth_links = advancedNegotiation(tempTopologyLinks, tempConfigSenderLinks, tempConfigReceiverLinks,
                                                      'bandwidth', randomizeLinkCapacities=True)
        aggregated_results_band_neg_links_modified.append(neg_opt_bandwidth_links)

    plotAggregatedFlowsBandwidthNegotiation(aggregated_results_band_neg_links_modified)


def getResultsIngressRoutersRandomized(topology, configSender, configReceiver):
    aggregated_results_band_neg_ingress_routers_ISP_1_perc = []
    aggregated_results_band_neg_ingress_routers_ISP_10_perc = []
    aggregated_results_band_neg_ingress_routers_ISP_20_perc = []
    for i in range(0, NO_TOPOLOGIES_EXAMINED):
        tempTopologyIngressRoutersISP = topology.copy()
        tempConfigSenderIngressRoutersISP = createDeepCopyConfigSender(configSender)
        tempConfigReceiverIngressRoutersISP = createDeepCopyConfigReceiver(configReceiver)

        neg_opt_bandwidth_ing_ISP_1_perc = advancedNegotiation(tempTopologyIngressRoutersISP,
                                                               tempConfigSenderIngressRoutersISP,
                                                               tempConfigReceiverIngressRoutersISP,
                                                               'bandwidth', noIngressRoutersISP=1)
        neg_opt_bandwidth_ing_ISP_10_perc = advancedNegotiation(tempTopologyIngressRoutersISP,
                                                                tempConfigSenderIngressRoutersISP,
                                                                tempConfigReceiverIngressRoutersISP,
                                                                'bandwidth', noIngressRoutersISP=10)
        neg_opt_bandwidth_ing_ISP_20_perc = advancedNegotiation(tempTopologyIngressRoutersISP,
                                                                tempConfigSenderIngressRoutersISP,
                                                                tempConfigReceiverIngressRoutersISP,
                                                                'bandwidth', noIngressRoutersISP=20)
        aggregated_results_band_neg_ingress_routers_ISP_1_perc.append(neg_opt_bandwidth_ing_ISP_1_perc)
        aggregated_results_band_neg_ingress_routers_ISP_10_perc.append(neg_opt_bandwidth_ing_ISP_10_perc)
        aggregated_results_band_neg_ingress_routers_ISP_20_perc.append(neg_opt_bandwidth_ing_ISP_20_perc)

    return [aggregated_results_band_neg_ingress_routers_ISP_1_perc,
            aggregated_results_band_neg_ingress_routers_ISP_10_perc,
            aggregated_results_band_neg_ingress_routers_ISP_20_perc]


def plotBandwidthCDFIngressRoutersRandomized(topology, configSender, configReceiver):
    resultsIngressRoutersRand = getResultsIngressRoutersRandomized(topology, configSender, configReceiver)
    plotCDF(resultsIngressRoutersRand, labels=['1%', '10%', '20%'], xlabel='% of Traffic Sent out of Total Demand',
            ylabel='% of experiment runs', title='CDF of Bandwidth for Ingress Routers Chosen by Specific Percentages',
            path_save_figure="results/CDFBandwidthIngressRoutersISPRandomized" + topology_name + ".png",
            attribute='bandwidth')
    plotCDF(resultsIngressRoutersRand, labels=['Min. Delay IR. 1%', 'Med. Delay IR. 1%', 'Max. Delay IR. 1%',
                                               'Min. Delay IR. 10%', 'Med. Delay IR. 10%', 'Max. Delay IR. 10%',
                                               'Min. Delay IR. 20%', 'Med. Delay IR. 20%', 'Max. Delay IR. 20%'],
            xlabel='Delay (ms) ', ylabel='% of experiment runs', title='CDF of Latency for Ingress Routers Chosen by Specific Percentages',
            path_save_figure="results/CDFLatencyIngressRoutersISPRandomized" + topology_name + ".png",
            attribute='latency')
    return resultsIngressRoutersRand


def getResultsEgressRoutersRandomized(topology, configSender, configReceiver):
    aggregated_results_band_neg_egress_routers_hosts_1_perc = []
    aggregated_results_band_neg_egress_routers_hosts_10_perc = []
    aggregated_results_band_neg_egress_routers_hosts_20_perc = []
    for i in range(0, NO_TOPOLOGIES_EXAMINED):
        tempTopologyEgressRoutersHosts = topology.copy()
        tempConfigSenderIngressRoutersISP = createDeepCopyConfigSender(configSender)
        tempConfigReceiverIngressRoutersISP = createDeepCopyConfigReceiver(configReceiver)

        neg_opt_bandwidth_eg_hosts_1_perc = advancedNegotiation(tempTopologyEgressRoutersHosts,
                                                                tempConfigSenderIngressRoutersISP,
                                                                tempConfigReceiverIngressRoutersISP,
                                                                'bandwidth', noEgressRoutersHosts=1, maxCap=1)
        neg_opt_bandwidth_eg_hosts_10_perc = advancedNegotiation(tempTopologyEgressRoutersHosts,
                                                                 tempConfigSenderIngressRoutersISP,
                                                                 tempConfigReceiverIngressRoutersISP,
                                                                 'bandwidth', noEgressRoutersHosts=10, maxCap=1)
        neg_opt_bandwidth_eg_hosts_20_perc = advancedNegotiation(tempTopologyEgressRoutersHosts,
                                                                 tempConfigSenderIngressRoutersISP,
                                                                 tempConfigReceiverIngressRoutersISP,
                                                                 'bandwidth', noEgressRoutersHosts=20, maxCap=1)
        aggregated_results_band_neg_egress_routers_hosts_1_perc.append(neg_opt_bandwidth_eg_hosts_1_perc)
        aggregated_results_band_neg_egress_routers_hosts_10_perc.append(neg_opt_bandwidth_eg_hosts_10_perc)
        aggregated_results_band_neg_egress_routers_hosts_20_perc.append(neg_opt_bandwidth_eg_hosts_20_perc)

    return [aggregated_results_band_neg_egress_routers_hosts_1_perc,
            aggregated_results_band_neg_egress_routers_hosts_10_perc,
            aggregated_results_band_neg_egress_routers_hosts_20_perc]


def getResultsTrafficRandomized(topology, configSender, configReceiver):
    aggregated_results_band_neg_traffic_half_maxcap = []
    aggregated_results_band_neg_traffic_maxcap = []
    aggregated_results_band_neg_traffic_twice_maxcap = []

    for i in range(0, NO_TOPOLOGIES_EXAMINED):
        tempTopologyTraffic = topology.copy()
        tempConfigSenderTraffic = createDeepCopyConfigSender(configSender)
        tempConfigReceiverTraffic = createDeepCopyConfigReceiver(configReceiver)

        neg_opt_bandwidth_traffic_half_maxcap = advancedNegotiation(tempTopologyTraffic, tempConfigSenderTraffic,
                                                                    tempConfigReceiverTraffic, 'bandwidth', maxCap=1/2)
        neg_opt_bandwidth_traffic_maxcap = advancedNegotiation(tempTopologyTraffic, tempConfigSenderTraffic,
                                                               tempConfigReceiverTraffic, 'bandwidth', maxCap=1)
        neg_opt_bandwidth_traffic_twice_maxcap = advancedNegotiation(tempTopologyTraffic, tempConfigSenderTraffic,
                                                                     tempConfigReceiverTraffic, 'bandwidth', maxCap=2)
        aggregated_results_band_neg_traffic_half_maxcap.append(neg_opt_bandwidth_traffic_half_maxcap)
        aggregated_results_band_neg_traffic_maxcap.append(neg_opt_bandwidth_traffic_maxcap)
        aggregated_results_band_neg_traffic_twice_maxcap.append(neg_opt_bandwidth_traffic_twice_maxcap)

    return [aggregated_results_band_neg_traffic_half_maxcap,
            aggregated_results_band_neg_traffic_maxcap,
            aggregated_results_band_neg_traffic_twice_maxcap]


def plotBandwidthCDFEgressRoutersRandomized(topology, configSender, configReceiver):
    resultsEgressRoutersRand = getResultsEgressRoutersRandomized(topology, configSender, configReceiver)
    plotCDF(resultsEgressRoutersRand, labels=['1%', '10%', '20%'], xlabel='% of Traffic Sent out of Total Demand',
            ylabel='% of experiment runs', title='CDF of Egress Routers Chosen by Specific Percentages',
            path_save_figure="results/CDFBandwidthEgressRoutersHostsRandomized" + topology_name + ".png",
            attribute='bandwidth')
    plotCDF(resultsEgressRoutersRand, labels=['Min. Delay EG. 1%', 'Med. Delay EG. 1%', 'Max. Delay EG. 1%',
                                              'Min. Delay EG. 10%', 'Med. Delay EG. 10%', 'Max. Delay EG. 10%',
                                              'Min. Delay EG. 20%', 'Med. Delay EG. 20%', 'Max. Delay EG. 20%'],
            xlabel='Delay (ms)', ylabel='% of experiment runs', title='CDF of Latency for Egress Routers Chosen by Specific Percentages',
            path_save_figure="results/CDFLatencyEgressRoutersHostsRandomized" + topology_name + ".png",
            attribute='latency')
    return resultsEgressRoutersRand


def plotBandwidthCDFTrafficRandomized(topology, configSender, configReceiver):
    resultsTrafficRand = getResultsTrafficRandomized(topology, configSender, configReceiver)
    plotCDF(resultsTrafficRand, labels=['Traffic = 1/2 * maxCap', 'Traffic = 1 maxCap', 'Traffic = 2 * maxCap'],
            xlabel='% of Traffic Sent out of Total Demand', ylabel='% of experiment runs',
            title='CDF of Traffic', path_save_figure="results/CDFBandwidthTrafficRandomized" + topology_name + ".png",
            attribute='bandwidth')
    plotCDF(resultsTrafficRand, labels=['Min. Delay 1/2* maxCap', 'Med. Delay 1/2 * maxCap',
                                        'Max. Delay 1/2 * maxCap', 'Min. Delay maxCap', 'Med. Delay maxCap',
                                        'Max. Delay maxCap', 'Min. Delay 2 * maxCap', 'Med. Delay 2 * maxCap',
                                        'Max. Delay 2 * maxCap'], xlabel='Delay (ms)', ylabel='% of experiment runs',
            title='CDF of Traffic', path_save_figure="results/CDFLatencyTrafficRandomized" + topology_name + ".png",
            attribute='latency')
    return resultsTrafficRand


def plotBandwidthCDFIngressesEgressesTrafficRandomized(topology, configSender, configReceiver):

    tempTopology = set_new_topology(topology, configSender, configReceiver, randomizeLinkCapacities=True,
                                    noIngressRoutersISP=10, noEgressRoutersHosts=10, maxCap=1)

    resultsIngressRoutersRandomized = plotBandwidthCDFEgressRoutersRandomized(tempTopology, configSender, configReceiver)
    resultsEgressRoutersRandomized = plotBandwidthCDFIngressRoutersRandomized(tempTopology, configSender, configReceiver)
    resultsTrafficRandomized = plotBandwidthCDFTrafficRandomized(tempTopology, configSender, configReceiver)
    plotCDF(resultsIngressRoutersRandomized + resultsEgressRoutersRandomized + resultsTrafficRandomized,
            labels=['1% Ingress', '10% Ingress', '20% Ingress', '1% Egress', '10% Egress', '20% Egress', '1/2 * maxCap',
                    'maxCap', '2 * maxCap'], xlabel='% of Traffic Sent out of Total Demand', ylabel='% of experiment runs',
            title="CDF of Randomized Ingress Routers, Egress Routers and Traffic at Specific Points",
            path_save_figure="results/CDFBandwidthIngressesEgressesTraffic" + topology_name + ".png",
            attribute='bandwidth')

    aggregatedResultsIngressRoutersRandomized = [item for sublist in resultsIngressRoutersRandomized for item in sublist]
    aggregatedResultsEgressRoutersRandomized = [item for sublist in resultsEgressRoutersRandomized for item in sublist]
    aggregatedResultsTrafficRandomized = [item for sublist in resultsTrafficRandomized for item in sublist]
    plotCDF([aggregatedResultsIngressRoutersRandomized, aggregatedResultsEgressRoutersRandomized,
             aggregatedResultsTrafficRandomized], labels=['Agg. Flows Ingress Routers',
                                                          'Agg. Flows Egress Routers',
                                                          'Agg. Flows Traffic'],
            xlabel='% of Traffic Sent out of Total Demand', ylabel='% of experiment runs',
            title='CDF of Aggregated Flows for Ingress Routers, Egress Routers and Traffic',
            path_save_figure="results/CDFBandwidthAggregatedFlowsIngressesEgressesTraffic" + topology_name + ".png",
            attribute='bandwidth')


def set_new_topology(topology, configSender, configReceiver, randomizeLinkCapacities, noIngressRoutersISP,
                     noEgressRoutersHosts, maxCap):
    createGraph(topology, configSender, configReceiver, randomizeLinkCapacities=randomizeLinkCapacities,
                noIngressRoutersISP=noIngressRoutersISP, noEgressRoutersHosts=noEgressRoutersHosts,
                maxCapacity=maxCap)
    return topology


def getStrategiesAggregatedResults(topology, configSender, configReceiver):

    aggregated_results_latency_opt = []
    aggregated_results_latency_opt_comm = []
    aggregated_results_band_opt_comm = []
    aggregated_results_band_opt = []
    aggregated_results_neg_opt_latency = []
    aggregated_results_neg_opt_bandwidth = []

    for i in range(0, NO_TOPOLOGIES_EXAMINED):
        topology = set_new_topology(topology, configSender, configReceiver, randomizeLinkCapacities=True,
                                    noIngressRoutersISP=10, noEgressRoutersHosts=10, maxCap=1)
        tempTopology = topology.copy()
        tempConfigSenderTraffic = createDeepCopyConfigSender(configSender)
        tempConfigReceiverTraffic = createDeepCopyConfigReceiver(configReceiver)
        latency_opt = configurationRunShortestLatencyPaths(tempTopology, tempConfigSenderTraffic,
                                                           tempConfigReceiverTraffic)
        latency_opt_comm = optimizerCommunicationAdvanced(tempTopology, tempConfigSenderTraffic,
                                                          tempConfigReceiverTraffic, 'latency')
        band_opt = configurationRunBandwidth(tempTopology, tempConfigSenderTraffic, tempConfigReceiverTraffic)
        band_opt_comm = optimizerCommunicationAdvanced(tempTopology, tempConfigSenderTraffic,
                                                       tempConfigReceiverTraffic, 'bandwidth')
        neg_opt_latency = advancedNegotiation(tempTopology, tempConfigSenderTraffic, tempConfigReceiverTraffic,
                                              'latency')
        neg_opt_bandwidth = advancedNegotiation(tempTopology, tempConfigSenderTraffic, tempConfigReceiverTraffic,
                                                'bandwidth')
        aggregated_results_latency_opt.append(latency_opt)
        aggregated_results_latency_opt_comm.append(latency_opt_comm)
        aggregated_results_band_opt.append(band_opt)
        aggregated_results_band_opt_comm.append(band_opt_comm)
        aggregated_results_neg_opt_latency.append(neg_opt_latency)
        aggregated_results_neg_opt_bandwidth.append(neg_opt_bandwidth)

    return [aggregated_results_latency_opt, aggregated_results_latency_opt_comm, aggregated_results_band_opt,
            aggregated_results_band_opt_comm, aggregated_results_neg_opt_latency, aggregated_results_neg_opt_bandwidth]


def plotBandwidthCDFStrategiesCompared(resultsStrategies, title_band, path_save_figure_band, title_lat,
                                       path_save_figure_lat):
    plotCDF(resultsStrategies, labels=['Lat Opt. no Comm.', 'Lat Opt. Comm', 'Band Opt. no Comm.', 'Band Op. Comm',
                                       'Lat Opt. Negotiate', 'Band Opt. Negotiate'],
            xlabel='% of Traffic Sent out of Total Demand', ylabel='% of experiment runs', title=title_band,
            path_save_figure=path_save_figure_band, attribute='bandwidth')
    plotCDF(resultsStrategies, labels=['Min Del Lat Opt', 'Med Del Lat Opt', 'Max Del Lat Opt',
                                       'Min Del Lat Opt Comm', 'Med Del Lat Opt Comm', 'Max Del Lat Opt Comm',
                                       'Min Del Band Opt', 'Med Del Band Opt', 'Max Del Band Opt',
                                       'Min Del Band Opt Comm', 'Med Del Band Opt Comm', 'Max Del Band Opt Comm',
                                       'Min Del Neg Lat', 'Med Del Neg Lat', 'Max Del Neg Lat',
                                       'Min Del Neg Band', 'Med Del Neg Band', 'Max Del Neg Band'],
            xlabel='Delay (ms)', ylabel='% of experiment runs', title=title_lat, path_save_figure=path_save_figure_lat,
            attribute='latency')


def plotResultsAllStrategiesOneTopology(topology, configSender, configReceiver):

    latency_opt = configurationRunShortestLatencyPaths(topology, configSender, configReceiver)
    latency_opt_comm = optimizerCommunicationAdvanced(topology, configSender, configReceiver, 'latency')
    band_opt_comm = optimizerCommunicationAdvanced(topology, configSender, configReceiver, 'bandwidth')
    band_opt = configurationRunBandwidth(topology, configSender, configReceiver)
    neg_opt_latency = advancedNegotiation(topology, configSender, configReceiver, 'latency')
    neg_opt_bandwidth = advancedNegotiation(topology, configSender, configReceiver, 'bandwidth')

    plotResults(latency_opt, latency_opt_comm, neg_opt_latency, band_opt_comm, band_opt, neg_opt_bandwidth,
                topology_name)
    plotResultsLatencyOptimizers(latency_opt, latency_opt_comm, neg_opt_latency, topology_name)
    plotResultsBandwidthOptimizers(band_opt, band_opt_comm, neg_opt_bandwidth, topology_name)


def setUpTopology(topology_name=topology_name):
    configSender = ConfigSender(file="", reading="", traffic={}, destination_hosts=[], egress_points=[])
    configReceiver = ConfigReceiver(file="", reading="", routers=[], hosts=[], ingress_points=[], links=[])
    readConfigSender("../configurations/configSender" + topology_name + ".txt", configSender)
    readConfigReceiver("../configurations/configReceiver" + topology_name + ".txt", configReceiver)
    topology = nx.Graph()

    randomizers.setLogFile(log_file)
    controllers.setRoutingResultsFile(results_file)

    return topology, configSender, configReceiver


def plotResultsOneTopology():
    topology, configSender, configReceiver = setUpTopology()
    resultsStrategies = getStrategiesAggregatedResults(topology, configSender, configReceiver)
    plotBandwidthCDFStrategiesCompared(resultsStrategies,
                                       title_band='CDF of Bandwidth Results on Multiple Topologies for all Strategies',
                                       path_save_figure_band="results/CDFBandwidthAllStrategies" + topology_name + ".png",
                                       title_lat='CDF of Latency Results on Multiple Topologies for all Strategies',
                                       path_save_figure_lat="results/CDFLatencyAllStrategies" + topology_name + ".png")
    plotBandwidthCDFIngressesEgressesTrafficRandomized(topology, configSender, configReceiver)


def plotResultsAllTopologies(topologies):
    global log_file
    global results_file

    all_top_results_lat_opt = []
    all_top_results_lat_opt_comm = []
    all_top_results_band_opt = []
    all_top_results_band_opt_comm = []
    all_top_results_neg_lat = []
    all_top_results_neg_band = []

    for topology_file in topologies:
        log_file = "results/log" + topology_file + ".txt"
        results_file = "results/routingResults" + topology_file + ".txt"
        topology, configSender, configReceiver = setUpTopology(topology_name=topology_file)
        results = getStrategiesAggregatedResults(topology, configSender, configReceiver)
        all_top_results_lat_opt += results[0]
        all_top_results_lat_opt_comm += results[1]
        all_top_results_band_opt += results[2]
        all_top_results_band_opt_comm += results[3]
        all_top_results_neg_lat += results[4]
        all_top_results_neg_band += results[5]

    results_strategies = [all_top_results_lat_opt, all_top_results_lat_opt_comm, all_top_results_band_opt,
                          all_top_results_neg_lat, all_top_results_band_opt_comm, all_top_results_neg_band]
    plotBandwidthCDFStrategiesCompared(results_strategies,
                                       title_band='CDF of Bandwidth Results on All ASes for No Communication and '
                                                  'Negotiation strategies',
                                       path_save_figure_band="results/CDFBandwidthNoCommVsNegAllASes.png",
                                       title_lat='CDF of Latency Results on All ASes for all Strategies',
                                       path_save_figure_lat="results/CDFLatencyNoCommVsNegAllASes.png")


def plotSimpleResults():
    topology, configSender, configReceiver = setUpTopology()
    plotResultsAllStrategiesOneTopology(topology, configSender, configReceiver)


def plotSimpleResultsLargeTopology():
    topology, configSender, configReceiver = setUpTopology()
    topology = set_new_topology(topology, configSender, configReceiver, randomizeLinkCapacities=True,
                                    noIngressRoutersISP=10, noEgressRoutersHosts=10, maxCap=2)
    plotResultsAllStrategiesOneTopology(topology, configSender, configReceiver)


class ConfigurationRun:
    if __name__ == "__main__":
        # plotSimpleResults()
        # plotSimpleResultsLargeTopology()
        plotResultsOneTopology()
        # plotResultsAllTopologies(all_topologies)


