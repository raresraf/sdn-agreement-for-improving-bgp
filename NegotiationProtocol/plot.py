import math
import numpy as np
import seaborn as sns
import statistics
from matplotlib import pyplot as plt

NUM_COLORS = 18


def plotResults(results_latency_opt, results_latency_opt_with_comm, results_negotiation_latency,
                results_bandwidth_opt_with_comm, results_bandwidth_opt, results_negotiation_bandwidth,
                topology_name):
    latency_opt_del = []
    latency_opt_comm_del = []
    bandwidth_opt_comm_cap = []
    bandwidth_opt_comm_del = []
    band_opt_del = []
    neg_bandwidth_del = []
    neg_latency_del =[]
    latency_opt_cap = []
    latency_opt_comm_cap = []
    band_opt_cap = []
    neg_bandwidth_cap = []
    neg_latency_cap = []
    max_val = 0
    xticks = []

    for i in results_latency_opt:
        if i.latency != math.inf:
            max_val = max(max_val, i.latency)
    for i in results_latency_opt_with_comm:
        if i.latency != math.inf:
            max_val = max(max_val, i.latency)
    for i in results_negotiation_latency:
        if i.latency != math.inf:
            max_val = max(max_val, i.latency)
    for i in results_bandwidth_opt_with_comm:
        if i.latency != math.inf:
            max_val = max(max_val, i.latency)
    for i in results_bandwidth_opt:
        if i.latency != math.inf:
            max_val = max(max_val, i.latency)
    for i in results_negotiation_bandwidth:
        if i.latency != math.inf:
            max_val = max(max_val, i.latency)

    if 0 <= max_val < 10:
        val_to_represent_max = max_val + 20
    else:
        val_to_represent_max = max_val + 50
    for i in results_latency_opt:
        if i.latency != math.inf:
            latency_opt_del.append(-i.latency)
        else:
            latency_opt_del.append(-val_to_represent_max)
        latency_opt_cap.append(min(i.traffic, i.bottleneck))
        xticks.append(i.destination)
    for i in results_latency_opt_with_comm:
        if i.latency != math.inf:
            latency_opt_comm_del.append(-i.latency)
        else:
            latency_opt_comm_del.append(-val_to_represent_max)
        latency_opt_comm_cap.append(min(i.traffic, i.bottleneck))
        xticks.append(i.destination)
    for i in results_negotiation_latency:
        if i.latency != math.inf:
            neg_latency_del.append(-i.latency)
        else:
            neg_latency_del.append(-val_to_represent_max)
        neg_latency_cap.append(min(i.traffic, i.bottleneck))
        xticks.append(i.destination)
    for i in results_bandwidth_opt_with_comm:
        if i.latency != math.inf:
            bandwidth_opt_comm_del.append(-i.latency)
        else:
            bandwidth_opt_comm_del.append(-val_to_represent_max)
        bandwidth_opt_comm_cap.append(min(i.traffic, i.bottleneck))
        xticks.append(i.destination)
    for i in results_bandwidth_opt:
        if i.latency != math.inf:
            band_opt_del.append(-i.latency)
        else:
            band_opt_del.append(-val_to_represent_max)
        band_opt_cap.append(min(i.traffic, i.bottleneck))
        xticks.append(i.destination)
    for i in results_negotiation_bandwidth:
        if i.latency != math.inf:
            neg_bandwidth_del.append(-i.latency)
        else:
            neg_bandwidth_del.append(-val_to_represent_max)
        neg_bandwidth_cap.append(min(i.traffic, i.bottleneck))
        xticks.append(i.destination)

    fig = plt.figure(num=1, figsize=(20, 6), dpi=80, facecolor='w', edgecolor='k')
    ax = fig.add_subplot(111)

    color_list = ['lavender', 'violet', 'plum', 'SlateBlue',  'magenta', 'purple']

    gap = .8 / len([latency_opt_del])
    for i, row in enumerate([latency_opt_del]):
        X = np.arange(len(row))
        ax.bar(X * gap, row, width=gap, color=color_list[0], edgecolor='black', label="Latency Opt. without Communication")

    gap = .8 / len([latency_opt_cap])
    for i, row in enumerate([latency_opt_cap]):
        X = np.arange(len(row))
        ax.bar(X * gap, row, width=gap, color=color_list[0], edgecolor='black' )

    last_gap = (len(latency_opt_del)+1) * gap

    gap = .8 / len([latency_opt_comm_del])
    for i, row in enumerate([latency_opt_comm_del]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[1], edgecolor='black', label="Latency Opt. with Communication")

    gap = .8 / len([latency_opt_comm_cap])
    for i, row in enumerate([latency_opt_comm_cap]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[1], edgecolor='black')

    last_gap = last_gap + (len(latency_opt_comm_del) + 1) * gap

    gap = .8 / len([neg_latency_del])
    for i, row in enumerate([neg_latency_del]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[2], edgecolor='black',
               label="Latency Negotiation")

    gap = .8 / len([neg_latency_cap])
    for i, row in enumerate([neg_latency_cap]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[2], edgecolor='black')

    last_gap = last_gap + (len(neg_latency_cap) + 1) * gap

    gap = .8 / len([band_opt_del])
    for i, row in enumerate([band_opt_del]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[3], edgecolor='black', label="Bandwidth Opt. without Communication")

    gap = .8 / len([band_opt_cap])
    for i, row in enumerate([band_opt_cap]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[3], edgecolor='black')

    last_gap = last_gap + (len(band_opt_cap) + 1) * gap

    gap = .8 / len([bandwidth_opt_comm_del])
    for i, row in enumerate([bandwidth_opt_comm_del]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[4], edgecolor='black',
               label="Bandwidth Opt. with Communication")

    gap = .8 / len([bandwidth_opt_comm_cap])
    for i, row in enumerate([bandwidth_opt_comm_cap]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[4], edgecolor='black')

    last_gap = last_gap + (len(bandwidth_opt_comm_del) + 1) * gap
    gap = .8 / len([neg_bandwidth_del])
    for i, row in enumerate([neg_bandwidth_del]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[5], edgecolor='black', label="Bandwidth Negotiation")

    gap = .8 / len([neg_bandwidth_cap])
    for i, row in enumerate([neg_bandwidth_cap]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[5], edgecolor='black')

    ax.legend(loc=2, bbox_to_anchor=(0.01, -0.1), ncol=3)

    ax.axhline(0, color='black')
    plt.xlabel('Optimizers results')
    plt.ylabel('Latency in Ms vs. Bandwidth in Gbps')
    plt.title('Comparison of Optimizers Results')

    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.2)

    locs = ax.get_yticks()
    labels = []
    for loc in locs:
        if loc < 0:
            labels.append(-loc)
        else:
            labels.append(loc)
    labels[0] = ' '
    if labels[1] > val_to_represent_max:
        labels[1] = ' '
    plt.yticks(locs, labels)

    ax.axhline(y=-val_to_represent_max, color='gray', linestyle='--', label="Congestion")

    plt.savefig("results/comparisonOfOptimizationsStrategies" + topology_name + ".png")
    plt.show()


def plotResultsBandwidthOptimizers(results_bandwidth_opt, results_bandwidth_opt_with_comm, results_bandwidth_negotiation,
                                   topology_name):
    band_opt_del = []
    band_opt_cap = []
    band_opt_with_comm_del = []
    band_opt_with_comm_cap = []
    neg_del = []
    neg_cap = []
    xticks = []
    max_val = 0

    for i in results_bandwidth_opt:
        if i.latency != math.inf:
            max_val = max(max_val, i.latency)
    for i in results_bandwidth_opt_with_comm:
        if i.latency != math.inf:
            max_val = max(max_val, i.latency)
    for i in results_bandwidth_negotiation:
        if i.latency != math.inf:
            max_val = max(max_val, i.latency)

    if 0 <= max_val < 10:
        val_to_represent_max = max_val + 20
    else:
        val_to_represent_max = max_val + 50

    for i in results_bandwidth_opt:
        if i.latency != math.inf:
            band_opt_del.append(-i.latency)
        else:
            band_opt_del.append(-val_to_represent_max)
        band_opt_cap.append(min(i.traffic, i.bottleneck))
        xticks.append(i.destination)
    for i in results_bandwidth_opt_with_comm:
        if i.latency != math.inf:
            band_opt_with_comm_del.append(-i.latency)
        else:
            band_opt_with_comm_del.append(-val_to_represent_max)
        band_opt_with_comm_cap.append(min(i.traffic, i.bottleneck))
        xticks.append(i.destination)
    for i in results_bandwidth_negotiation:
        if i.latency != math.inf:
            neg_del.append(-i.latency)
        else:
            neg_del.append(-val_to_represent_max)
        neg_cap.append(min(i.traffic, i.bottleneck))
        xticks.append(i.destination)

    fig = plt.figure(num=1, figsize=(10, 6), dpi=80, facecolor='w', edgecolor='k')
    ax = fig.add_subplot(111)
    ax.axhline(y=-val_to_represent_max, color='gray', linestyle='--', label="Congestion")

    color_list = ['lavender', 'violet', 'plum']

    X = []
    xticksPos = []
    gap = .8 / len([band_opt_del])
    for i, row in enumerate([band_opt_del]):
        X = np.arange(len(row))
        ax.bar(X * gap, row, width=gap, color=color_list[0], edgecolor='black', label="Bandwidth Opt. without Communication")
    for i in X:
        xticksPos.append(i * gap)

    gap = .8 / len([band_opt_cap])
    for i, row in enumerate([band_opt_cap]):
        X = np.arange(len(row))
        ax.bar(X * gap, row, width=gap, color=color_list[0], edgecolor='black', tick_label=xticks[i])
        i += 1

    last_gap = (len(band_opt_del) + 1) * gap

    gap = .8 / len([band_opt_with_comm_del])
    for i, row in enumerate([band_opt_with_comm_del]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[1], edgecolor='black', label="Bandwidth Opt. with Communication")
    for i in X:
        xticksPos.append(last_gap + i * gap)

    gap = .8 / len([band_opt_with_comm_cap])
    for i, row in enumerate([band_opt_with_comm_cap]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[1], edgecolor='black', tick_label=xticks[i])
        i += 1

    last_gap = last_gap + (len(band_opt_with_comm_cap) + 1) * gap

    gap = .8 / len([neg_del])
    for i, row in enumerate([neg_del]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[2], edgecolor='black', label="Bandwidth Negotiation")
    for i in X:
        xticksPos.append(last_gap + i * gap)

    gap = .8 / len([neg_cap])
    for i, row in enumerate([neg_cap]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[2], edgecolor='black', tick_label=xticks[i])
        i += 1

    ax.legend(loc=2, bbox_to_anchor=(-0.05, -0.15), ncol=3)

    locs = ax.get_yticks()
    labels = []
    for loc in locs:
        if loc < 0:
            labels.append(-loc)
        else:
            labels.append(loc)
    labels[0] = ' '
    if labels[1] > val_to_represent_max:
        labels[1] = ' '
    plt.yticks(locs, labels)
    plt.xticks(xticksPos, xticks, rotation=90, fontsize=10)

    ax.axhline(0, color='black')
    plt.ylabel('Latency in Ms vs. Bandwidth in Gbps')
    plt.title('Bandwidth Optimizers Results ')

    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.2)

    plt.savefig("results/comparisonOfBandwidthOptimizationsStrategies" + topology_name + ".png")
    plt.show()


def plotResultsLatencyOptimizers(results_latency_opt, results_latency_opt_with_comm, results_latency_negotiation,
                                 topology_name):
    latency_opt_del = []
    latency_opt_cap = []
    latency_opt_comm_del = []
    latency_opt_comm_cap = []
    latency_neg_del = []
    latency_neg_cap = []
    xticks = []
    max_val = 0

    for i in results_latency_opt:
        if i.latency != math.inf:
            max_val = max(max_val, i.latency)
    for i in results_latency_opt_with_comm:
        if i.latency != math.inf:
            max_val = max(max_val, i.latency)
    for i in results_latency_negotiation:
        if i.latency != math.inf:
            max_val = max(max_val, i.latency)

    if 0 <= max_val < 10:
        val_to_represent_max = max_val + 20
    else:
        val_to_represent_max = max_val + 50

    for i in results_latency_opt:
        if i.latency != math.inf:
            latency_opt_del.append(-i.latency)
        else:
            latency_opt_del.append(-val_to_represent_max)
        latency_opt_cap.append(min(i.traffic, i.bottleneck))
        xticks.append(i.destination)
    for i in results_latency_opt_with_comm:
        if i.latency != math.inf:
            latency_opt_comm_del.append(-i.latency)
        else:
            latency_opt_comm_del.append(-val_to_represent_max)
        latency_opt_comm_cap.append(min(i.traffic, i.bottleneck))
        xticks.append(i.destination)
    for i in results_latency_negotiation:
        if i.latency != math.inf:
            latency_neg_del.append(-i.latency)
        else:
            latency_neg_del.append(-val_to_represent_max)
        latency_neg_cap.append(min(i.traffic, i.bottleneck))
        xticks.append(i.destination)

    fig = plt.figure(num=1, figsize=(10, 6), dpi=80, facecolor='w', edgecolor='k')
    ax = fig.add_subplot(111)
    ax.axhline(y=-val_to_represent_max, color='gray', linestyle='--', label="Congestion")

    color_list = ['lightcyan', 'lightskyblue', 'steelblue']

    xticksPos = []
    X = []
    gap = .9 / len([latency_opt_del])
    for i, row in enumerate([latency_opt_del]):
        X = np.arange(len(row))
        ax.bar(X * gap, row, width=gap, color=color_list[0], edgecolor='black', label="Latency Opt. without Communication")
    for i in X:
        xticksPos.append(i * gap)

    gap = .9 / len([latency_opt_cap])
    for i, row in enumerate([latency_opt_cap]):
        X = np.arange(len(row))
        ax.bar(X * gap, row, width=gap, color=color_list[0], edgecolor='black')

    last_gap = (len(latency_opt_cap) + 1) * gap

    gap = .9 / len([latency_opt_comm_del])
    for i, row in enumerate([latency_opt_comm_del]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[1], edgecolor='black', label="Latency Opt. with Communication")
    for i in X:
        xticksPos.append(last_gap + i * gap)

    gap = .9 / len([latency_opt_comm_cap])
    for i, row in enumerate([latency_opt_comm_cap]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[1], edgecolor='black')

    last_gap = last_gap + (len(latency_opt_comm_cap) + 1) * gap

    gap = .9 / len([latency_neg_del])
    for i, row in enumerate([latency_neg_del]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[2], edgecolor='black', label="Latency Negotiation")
    for i in X:
        xticksPos.append(last_gap + i * gap)

    gap = .9 / len([latency_neg_cap])
    for i, row in enumerate([latency_neg_cap]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[2], edgecolor='black')

    ax.legend(loc=2, bbox_to_anchor=(-0.02, -0.15), ncol=3)

    locs = ax.get_yticks()
    labels = []
    for loc in locs:
        if loc < 0:
            labels.append(-loc)
        else:
            labels.append(loc)
    labels[0] = ' '
    if labels[1] > val_to_represent_max:
        labels[1] = ' '
    plt.yticks(locs, labels)
    plt.xticks(xticksPos, xticks, rotation=90, fontsize=10)

    ax.axhline(0, color='black')
    plt.ylabel('Latency in Ms vs. Bandwidth in Gbps')
    plt.title('Latency Optimizers Results')

    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.2)

    plt.savefig("results/comparisonOfLatencyOptimizationsStrategies" + topology_name + ".png")
    plt.show()


def plotAggregatedFlowsBandwidthNegotiation(agg_neg_opt_band):
    sns.set()
    sns.set_palette("husl", 8)
    fig, ax = plt.subplots()

    for result in agg_neg_opt_band:
        result.sort(key=lambda el: el.latency)
    agg_latencies = []
    agg_bandwidths = []
    for result in agg_neg_opt_band:
        latency = [b.latency for b in result]
        bandwidth = [min(d.traffic, d.bottleneck) for d in result]
        agg_latencies.append(latency)
        agg_bandwidths.append(bandwidth)

    for i, el in enumerate(agg_latencies):
        ax.plot(el, agg_bandwidths[i], label="topo{}".format(i+1), marker='.', linestyle='-')
    ax.legend()
    ax.set_xlabel('Delay')
    ax.set_ylabel('Bandwidth')
    ax.set_title('Aggregated Flows for Bandwidth Negotiation')
    plt.show()


def extractBandwidthSent(list_result):
    total_band_sent = 0
    for route in list_result:
        if route.latency != math.inf:
            total_band_sent += min(route.bottleneck, route.traffic)
    return total_band_sent


def extractPercTrafficSent(list_result):
    total_band_sent = 0
    total_traffic_to_be_sent = 0
    for route in list_result:
        if route.latency != math.inf:
            total_band_sent += min(route.bottleneck, route.traffic)
        total_traffic_to_be_sent += route.traffic
    return total_band_sent/total_traffic_to_be_sent * 100


def extractLatencies(list_result):
    min_latency = min((route.latency for route in list_result if route.latency != math.inf), default=0)
    max_latency = max((route.latency for route in list_result if route.latency != math.inf), default=0)
    #list_result = [np.NaN for i in list_result if i.latency == math.inf]
    #median_latency = np.nanmedian(list_result)
    median_latency = statistics.median(route.latency for route in list_result if route.latency != math.inf)
    return min_latency, median_latency, max_latency


def plotCDF(list_results, labels, xlabel, ylabel, title, path_save_figure, attribute):
    results = []

    for topologies_for_percentage in list_results:
        if attribute is 'bandwidth':
            routers_x_perc = []
            for topology in topologies_for_percentage:
                routers_x_perc.append(extractPercTrafficSent(topology))
            results.append(routers_x_perc)
        if attribute is 'latency':
            min_delay_all, median_delay_all, max_delay_all = [], [], []
            for topology in topologies_for_percentage:
                min_del, median_del, max_del = extractLatencies(topology)
                min_delay_all.append(min_del)
                median_delay_all.append(median_del)
                max_delay_all.append(max_del)
            results.append(min_delay_all)
            results.append(median_delay_all)
            results.append(max_delay_all)

    fig = plt.figure(num=1, figsize=(8, 8), facecolor='w', edgecolor='k')
    ax = fig.add_subplot(111)
    linestyles = ['-', '-', '-', '-', '-', '-', '--','--', '--', '--', '--', '--', ':', ':', ':', ':', ':', ':']
    linestylesBandwidth = ['-', '--', ':', 'solid', (0, (3, 10, 1, 10)), (0, (3, 10, 1, 10, 1, 10)), (0, (1, 1)), (0, (3, 1, 1, 1, 1, 1)), (0, (3, 5, 1, 5, 1, 5))]
    linestylesBandwidth = ['solid', ':', 'solid', ':', 'solid', ':']
    # cm = plt.get_cmap('gist_rainbow')
    # ax.set_prop_cycle('color', [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

    #for i, result in enumerate(results):
     #   ax.hist(result, density=True, cumulative=True, label=labels[i], alpha=0.6, color=colors[i%5],
      #          linewidth=1)

    colors_band = ['blue', 'red', 'orange', 'green', 'purple', 'violet', 'yellow', 'brown', 'grey', 'plum']
    colors_lat = ['blue', 'blue', 'blue', 'red', 'red', 'red', 'green', 'green', 'green', 'orange', 'orange', 'orange',
                  'purple', 'purple', 'purple', 'violet', 'violet', 'violet']

    for i, result in enumerate(results):
        x = np.sort(result)
        y = np.arange(1, len(result) + 1) / len(result)
        if attribute == 'latency':
            ax.plot(x, y, marker='.', label=labels[i], linestyle=linestyles[i], alpha=0.7)
        else:
            ax.plot(x, y, marker='.', label=labels[i], linestyle=linestylesBandwidth[i % 6], alpha=0.9)

    # ax.legend(bbox_to_anchor=(0, -0.1), loc=2, ncol=3)
    ax.legend(bbox_to_anchor=(0, -0.1), loc=2, ncol=3)

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    # ax.set_title(title)
    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.32)
    plt.savefig(path_save_figure)
    plt.show()

