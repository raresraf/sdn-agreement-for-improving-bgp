def transform_repetitia_topology(file_to_transform, output_file):
    o = open(output_file, 'w')
    f = open(file_to_transform, 'r')
    line = f.readline()
    while line:
        parts = line.split(" ")
        newFormatLine = "r" + parts[1] + "-r" + parts[2] + ": " + str(int(int(parts[4])/100000)) + "/" + parts[5]
        o.write(newFormatLine)
        line = f.readline()
