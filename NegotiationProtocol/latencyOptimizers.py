import math

from NegotiationProtocol.controllers import createGraph, findRouterForDestination, getShortestRouteDijkstra, \
    findBottleneck, Route, findSharedLinks, createDeepCopyConfigSender, createDeepCopyConfigReceiver, findDelayRoute


def configurationRunShortestLatencyPaths(topology, configSender, configReceiver, randomizeLinkCapacities=False,
                                         noEgressPointsHosts=None, noIngressPointsISP=None, maxCap=0):
    print("Results for shortest paths based on latency without communication with ISP: ")
    routes = []

    createGraph(topology, configSender, configReceiver, randomizeLinkCapacities, noEgressPointsHosts,
                noIngressPointsISP, maxCap)

    tempConfigSender = createDeepCopyConfigSender(configSender)
    tempConfigReceiver = createDeepCopyConfigReceiver(configReceiver)

    tempTopology = topology.copy()

    results_latency_opt = []

    for destination in tempConfigSender.destination_hosts:
        router_destination = findRouterForDestination(tempConfigReceiver, destination)
        shortest_path = getShortestRouteDijkstra(tempConfigReceiver, tempTopology, router_destination, 'latency')
        traffic = tempConfigSender.traffic[destination]
        bottleneck = findBottleneck(tempTopology, shortest_path[1])
        route = Route(route=shortest_path[1], bottleneck=min(traffic, bottleneck), traffic=traffic,
                      latency=shortest_path[0],
                      destination=destination)
        if traffic > bottleneck:
            route.latency = math.inf
        results_latency_opt.append(route)
        routes.append(route)

    sharedLinks = findSharedLinks(routes)
    for sharedLink in sharedLinks:
        routers = sharedLink.link.split('->')
        linkCapacity = tempTopology[routers[0]][routers[1]]['bandwidth']
        traffic = 0.0
        for route in sharedLink.routesSharingLink:
            traffic += route.traffic
            if traffic > linkCapacity:
                list(filter(lambda el: (el.destination == route.destination), results_latency_opt))[
                    0].latency = math.inf
                list(filter(lambda el: (el.destination == route.destination), results_latency_opt))[0].bottleneck = \
                    min(min(route.traffic, route.bottleneck), linkCapacity / len(sharedLink.routesSharingLink))

    for route in results_latency_opt:
        print(
            "For destination {} we send {} GB with {} delay".format(route.destination, route.bottleneck, route.latency))
    print("")

    return results_latency_opt


def deleteDemand(new_result, configSenderTemp, demandSatisfied):
    destinations = []
    for route in new_result:
        destinations.append(route.destination)
    for destination in destinations:
        if configSenderTemp.traffic[destination] == 0:
            del configSenderTemp.traffic[destination]
            del configSenderTemp.destination_hosts[configSenderTemp.destination_hosts.index(destination)]
            demandSatisfied = True
    return demandSatisfied


def optimizerCommunicationAdvanced(topology, configSender, configReceiver, attribute_optimizer,
                                   randomizeLinkCapacities=False, noEgressPointsHosts=None, noIngressPointsISP=None,
                                   maxCap=0):
    createGraph(topology, configSender, configReceiver, randomizeLinkCapacities, noEgressPointsHosts,
                noIngressPointsISP, maxCap)
    results_latency_opt_with_comm = []
    tempTopology = topology.copy()

    tempTopology = topology.copy()
    configSenderTemp = createDeepCopyConfigSender(configSender)
    configReceiverTemp = createDeepCopyConfigReceiver(configReceiver)

    paths_available_for_demands = {}
    for destination in configSenderTemp.destination_hosts:
        paths_available_for_demands[destination] = True

    found_paths = True
    while len(configSenderTemp.traffic) > 0 and found_paths == True:
        demandSatisfied = False
        while demandSatisfied == False and found_paths == True:
            new_result = configurationRunShortestPathsCommunication(tempTopology, configSenderTemp,
                                                                    configReceiverTemp, attribute_optimizer)
            for result in new_result:
                if result.bottleneck == -1:
                    paths_available_for_demands[result.destination] = False
                    del configSenderTemp.destination_hosts[configSenderTemp.destination_hosts.index(result.destination)]
                    found_paths = False
                    for i, elem in enumerate(paths_available_for_demands):
                        if paths_available_for_demands[elem]:
                            found_paths = True
                            break
                    print("No more paths available to meet the rest of the demands for destination".format(
                        result.destination))
            found = False
            for i in range(0, len(new_result)):
                if new_result[i].bottleneck != -1:
                    try:
                        results_latency_opt_with_comm.append(new_result[i])
                        found = True
                    except:
                        continue
            if not found:
                found_paths = False
                break
            for result in new_result:
                configSenderTemp.traffic[result.destination] -= min(result.traffic, result.bottleneck)
                try:
                    for i, j in zip(result.route, result.route[1:]):
                        tempTopology[i][j]['bandwidth'] -= min(result.traffic, result.bottleneck)
                        if (tempTopology[i][j]['bandwidth']) == 0 or tempTopology[i][j]['bandwidth'] < 0:
                            tempTopology.remove_edge(i, j)  # if this results in negative weights, something is wrong
                except:
                    print("More shared links")
            demandSatisfied = deleteDemand(new_result, configSenderTemp, demandSatisfied)

    return results_latency_opt_with_comm


def configurationRunShortestPathsCommunication(topology, configSender, configReceiver, attribute_optimization):
    print("Results for shortest paths based on latency with communication with the ISP: ")
    routes = []
    results_obtained = []

    tempTopology = topology.copy()

    for destination in configSender.destination_hosts:
        router_destination = findRouterForDestination(configReceiver, destination)
        try:
                shortest_path = getShortestRouteDijkstra(configReceiver, tempTopology, router_destination,
                                                         attribute_optimization)
        except:
            route = Route(route=[], bottleneck=-1, traffic=-1, latency=-1, destination=destination)
            results_obtained.append(route)
            continue
        traffic = configSender.traffic[destination]
        bottleneck = findBottleneck(tempTopology, shortest_path[1])
        if attribute_optimization == 'latency':
            route = Route(route=shortest_path[1], bottleneck=min(bottleneck, traffic), traffic=traffic,
                          latency=shortest_path[0], destination=destination)
        else:
            delay = findDelayRoute(shortest_path[1], tempTopology)
            route = Route(route=shortest_path[1], bottleneck=min(bottleneck, traffic), traffic=traffic, latency=delay,
                          destination=destination)
        results_obtained.append(route)
        routes.append(route)

    sharedLinks = findSharedLinks(routes)
    print("Shared links :{}".format(sharedLinks))
    print("")
    for sharedLink in sharedLinks:
        routers = sharedLink.link.split('->')
        linkCapacity = tempTopology[routers[0]][routers[1]]['bandwidth']
        traffic = 0.0
        for route in sharedLink.routesSharingLink:
            traffic += route.traffic
            if traffic > linkCapacity:
                for route in sharedLink.routesSharingLink:
                    list(filter(lambda el: (el.destination == route.destination), results_obtained))[0].bottleneck = \
                        min(min(route.traffic, route.bottleneck), linkCapacity / len(sharedLink.routesSharingLink))

    for route in results_obtained:
        print(
            "For destination {} we send {} GB with {} delay".format(route.destination, route.bottleneck, route.latency))
    print("")

    return results_obtained
