import math
import networkx as nx
import unittest
from pathlib import Path

from NegotiationProtocol.CP import ConfigSender, readConfigSender
from NegotiationProtocol.ISP import ConfigReceiver, readConfigReceiver
from NegotiationProtocol.bandwidthOptimizers import OptionsForCP, computeOptionsForCP, selectOption, \
    findDelayMultipleRoutes, assignCapacityToLinkAfterSplitting, \
    configurationRunBandwidth, advancedNegotiation, configurationRunNegotiationProtocol
from NegotiationProtocol.controllers import createGraph, Route, findSharedLinks

path2Up = Path(__file__).parents[2]

topology_to_test = '_topReport'


class TestBandwidthOptimizers(unittest.TestCase):
    def setUp(self):
        configSender = ConfigSender(file="", reading="", traffic={}, destination_hosts=[], egress_points=[])
        configReceiver = ConfigReceiver(file="", reading="", routers=[], hosts=[], ingress_points=[], links=[])
        readConfigSender("{}/configurations/configSender".format(path2Up) + topology_to_test + ".txt", configSender)
        readConfigReceiver("{}/configurations/configReceiver".format(path2Up) + topology_to_test + ".txt",
                           configReceiver)
        topology = nx.Graph()
        self.configReceiver = configReceiver
        self.configSender = configSender
        createGraph(topology, self.configSender, self.configReceiver, randomizeLinkCapacities=False)
        self.topology = topology

    def test_computeOptionsForCP(self):
        route1 = Route(route=['r1', 'r2', 'r4', 'r5'], bottleneck=3.0, traffic=5.0, latency=6.0,
                       destination='2/8')
        route2 = Route(route=['r1', 'r2'], bottleneck=3.0, traffic=5.0, latency=2.0,
                       destination='1/8')
        sharedLinks = findSharedLinks([route1, route2])
        option1 = OptionsForCP(capacity=3.0, delay=6.0, route=['r1', 'r2', 'r4', 'r5'], destinations=['2/8'])
        option2 = OptionsForCP(capacity=3.0, delay=2.0, route= ['r1', 'r2'], destinations=['1/8'])
        options = computeOptionsForCP(sharedLinks, [route1, route2], self.topology)
        self.assertIn(option1, options)
        self.assertIn(option2, options)

    def test_selectOption(self):
        route1 = Route(route=['r1', 'r2', 'r4', 'r5'], bottleneck=3.0, traffic=5.0, latency=6.0,
                       destination='2/8')
        route2 = Route(route=['r1', 'r2'], bottleneck=3.0, traffic=5.0, latency=2.0,
                       destination='1/8')
        sharedLinks = findSharedLinks([route1, route2])
        option1 = OptionsForCP(capacity=3.0, delay=6.0, route=['r1', 'r2', 'r4', 'r5'], destinations=['2/8'])
        option2 = OptionsForCP(capacity=1.5, delay=6.0, route=[Route(route=['r1', 'r2', 'r4', 'r5'], bottleneck=3.0,
                                                                     traffic=5.0, latency=6.0, destination='2/8'),
                                                               Route(route=['r1', 'r2'], bottleneck=3.0,
                                                                     traffic=5.0, latency=2.0, destination='1/8')],
                               destinations=['2/8', '1/8'])
        option3 = OptionsForCP(capacity=3.0, delay=2.0, route=['r1', 'r2'], destinations=['1/8'])
        self.assertEqual(selectOption([option1, option2, option3]), option3)

    def test_findDelayMultipleRoutes(self):
        route1 = Route(route=['r1', 'r2', 'r4', 'r5'], bottleneck=3.0, traffic=5.0, latency=6.0,
                       destination='2/8')
        route2 = Route(route=['r1', 'r2'], bottleneck=3.0, traffic=5.0, latency=2.0,
                       destination='1/8')
        self.assertEqual(findDelayMultipleRoutes([route1,route2], self.topology), 6.0)
        self.assertEqual(findDelayMultipleRoutes([route2], self.topology), 2.0)

    def test_assignCapacityToLinkAfterSplitting(self):
        topology_after_splitting = assignCapacityToLinkAfterSplitting('r1->r2', 2, self.topology)
        self.assertEqual(topology_after_splitting['r1']['r2']['bandwidth'], 1.5)
        topology_after_splitting = assignCapacityToLinkAfterSplitting('r2->r4', 4, self.topology)
        self.assertEqual(topology_after_splitting['r2']['r4']['bandwidth'], 1)
        topology_after_splitting = assignCapacityToLinkAfterSplitting('r3->r5', 1, self.topology)
        self.assertEqual(topology_after_splitting['r3']['r5']['bandwidth'], 1)

    def test_configurationRunBandwidth(self):
        results = configurationRunBandwidth(self.topology, self.configSender, self.configReceiver)
        route1 = Route(bottleneck=2.5, destination='2/8', latency=math.inf, route=['r3', 'r2', 'r4', 'r5'], traffic=4.0)
        route2 = Route(bottleneck=2.5, destination='1/8', latency=math.inf, route=['r3', 'r2'], traffic=4.0)
        self.assertEqual(results, [route1, route2])

    def test_advancedNegotiation(self):
        results_band = advancedNegotiation(self.topology, self.configSender, self.configReceiver, attribute_negotiation=
                                           'bandwidth')
        route1 = Route(bottleneck=4.0, destination='1/8', latency=3.0, route=['r3', 'r2'], traffic=4.0)
        route2 = Route(bottleneck=3.0, destination='2/8', latency=6.0, route=['r1', 'r2', 'r4', 'r5'], traffic=3.0)
        route3 = Route(bottleneck=1.0, destination='2/8', latency=5.0, route=['r3', 'r5'], traffic=1.0)
        self.assertIn(route1, results_band)
        self.assertIn(route2, results_band)
        self.assertIn(route3, results_band)
        results_lat = advancedNegotiation(self.topology, self.configSender, self.configReceiver, attribute_negotiation=
                                          'latency')
        route1 = Route(bottleneck=3.0, destination='1/8', latency=2.0, route=['r1', 'r2'], traffic=3.0)
        route2 = Route(bottleneck=1.0, destination='1/8', latency=3.0, route=['r3', 'r2'], traffic=1.0)
        route3 = Route(bottleneck=1.0, destination='2/8', latency=5.0, route=['r3', 'r5'], traffic=1.0)
        route4 = Route(bottleneck=3.0, destination='2/8', latency=7.0, route=['r3', 'r2', 'r4', 'r5'], traffic=3.0)
        self.assertIn(route1, results_lat)
        self.assertIn(route2, results_lat)
        self.assertIn(route3, results_lat)
        self.assertIn(route4, results_lat)

    def test_configurationRunNegotiationProtocol(self):
        results = configurationRunNegotiationProtocol(self.topology, self.configSender, self.configReceiver, 'bandwidth')
        route = Route(bottleneck=4.0, destination='1/8', latency=3.0, route=['r3', 'r2'], traffic=4.0)
        self.assertEqual(results, (1, [route]))

        results = configurationRunNegotiationProtocol(self.topology, self.configSender, self.configReceiver,
                                                      'latency')
        route = Route(bottleneck=3.0, destination='1/8', latency=2.0, route=['r1', 'r2'], traffic=3.0)
        self.assertEqual(results, (1, [route]))


if __name__ == '__main__':
    unittest.main()

