import networkx as nx
import sys
import unittest
from pathlib import Path

from NegotiationProtocol.CP import ConfigSender, readConfigSender
from NegotiationProtocol.ISP import ConfigReceiver, readConfigReceiver
from NegotiationProtocol.controllers import createGraph, getShortestRouteDijkstra, findRouterForDestination, \
    findBottleneck, \
    findSharedLinksBetweenTwoRoutes, findSharedLinks, findDelayRoute, Route, SharedLink

path2Up = Path(__file__).parents[2]

topology_to_test = '_topReport'


class TestControllers(unittest.TestCase):
    def setUp(self):
        configSender = ConfigSender(file="", reading="", traffic={}, destination_hosts=[], egress_points=[])
        configReceiver = ConfigReceiver(file="", reading="", routers=[], hosts=[], ingress_points=[], links=[])
        readConfigSender("{}/configurations/configSender".format(path2Up) + topology_to_test + ".txt", configSender)
        readConfigReceiver("{}/configurations/configReceiver".format(path2Up) + topology_to_test + ".txt",
                           configReceiver)
        topology = nx.Graph()
        self.configReceiver = configReceiver
        self.configSender = configSender
        createGraph(topology, self.configSender, self.configReceiver, randomizeLinkCapacities=False)
        self.topology = topology

    def test_createGraph(self):
        self.assertTrue(self.topology.has_node('r1'))
        self.assertTrue(self.topology.has_node('r2'))
        self.assertTrue(self.topology.has_node('r3'))
        self.assertTrue(self.topology.has_node('r4'))
        self.assertTrue(self.topology.has_node('r5'))
        self.assertFalse(self.topology.has_node('r6'))
        self.assertTrue(self.topology.has_edge('r1', 'r2'))
        self.assertTrue(self.topology.has_edge('r2', 'r3'))
        self.assertTrue(self.topology.has_edge('r2', 'r4'))
        self.assertTrue(self.topology.has_edge('r3', 'r5'))
        self.assertTrue(self.topology.has_edge('r4', 'r5'))
        self.assertFalse(self.topology.has_edge('r2', 'r5'))
        self.assertEqual(self.topology['r2']['r4']['bandwidth'], 4.0)
        self.assertEqual(self.topology['r2']['r4']['latency'], 2.0)

    def test_getShortestRouteDijkstra(self):
        self.assertEqual(getShortestRouteDijkstra(self.configReceiver, self.topology, 'r5', 'bandwidth'), (0.65,
                                                                                                           ['r3', 'r2',
                                                                                                            'r4','r5']))
        self.assertEqual(getShortestRouteDijkstra(self.configReceiver, self.topology, 'r5', 'latency'), (5.0,
                                                                                                         ['r3','r5']))
        self.assertEqual(getShortestRouteDijkstra(self.configReceiver, self.topology, 'r3', 'latency'),
                         (0.0, ['r3']))

    def test_findRouterForDestination(self):
        self.assertEqual(findRouterForDestination(self.configReceiver, '2/8'), 'r5')
        self.assertEqual(findRouterForDestination(self.configReceiver, '1/8'), 'r2')
        self.assertEqual(findRouterForDestination(self.configReceiver, 'r1'), 'Not a host')

    def test_findBottleneck(self):
        self.assertEqual(findBottleneck(self.topology, ['r3', 'r5']), 1.0)
        self.assertEqual(findBottleneck(self.topology, ['r1', 'r2', 'r4', 'r5']), 3.0)
        self.assertEqual(findBottleneck(self.topology, ['r1']), sys.maxsize)

    def test_findSharedLinkBetweenTwoRoutes(self):
        route1 =  Route(route=['r1', 'r2', 'r3', 'r4'], bottleneck=2.0, traffic=5.0, latency=2.0,
                      destination='1/8')
        route2 = Route(route=['r2', 'r1', 'r5', 'r3', 'r4'], bottleneck=3.0, traffic=5.0, latency=2.0,
                       destination='2/8')
        self.assertEqual(findSharedLinksBetweenTwoRoutes(route1, route2), ['r1->r2', 'r3->r4'])

    def test_findSharedLinks(self):
        route1 = Route(route=['r1', 'r2', 'r3', 'r4'], bottleneck=2.0, traffic=5.0, latency=2.0,
                       destination='1/8')
        route2 = Route(route=['r2', 'r1', 'r5', 'r3', 'r4'], bottleneck=3.0, traffic=5.0, latency=2.0,
                       destination='2/8')
        self.assertEqual(findSharedLinks([route1, route2]), [SharedLink('r1->r2', [route1, route2]),
                                                             SharedLink('r3->r4', [route1, route2])])

    def test_findDelayRoute(self):
        self.assertEqual(findDelayRoute(['r1', 'r2', 'r4', 'r5'], self.topology), 6.0)
        self.assertEqual(findDelayRoute(['r1'], self.topology), 0.0)


if __name__ == '__main__':
    unittest.main()
