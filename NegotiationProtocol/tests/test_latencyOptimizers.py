import math
import networkx as nx
import unittest
from pathlib import Path

from NegotiationProtocol.CP import ConfigSender, readConfigSender
from NegotiationProtocol.ISP import ConfigReceiver, readConfigReceiver
from NegotiationProtocol.controllers import createGraph, Route, createDeepCopyConfigSender
from NegotiationProtocol.latencyOptimizers import configurationRunShortestLatencyPaths, \
    configurationRunShortestPathsCommunication, \
    deleteDemand, optimizerCommunicationAdvanced

path2Up = Path(__file__).parents[2]

topology_to_test = '_topReport'


class TestLatencyOptimizers(unittest.TestCase):
    def setUp(self):
        configSender = ConfigSender(file="", reading="", traffic={}, destination_hosts=[], egress_points=[])
        configReceiver = ConfigReceiver(file="", reading="", routers=[], hosts=[], ingress_points=[], links=[])
        readConfigSender("{}/configurations/configSender".format(path2Up) + topology_to_test + ".txt", configSender)
        readConfigReceiver("{}/configurations/configReceiver".format(path2Up) + topology_to_test + ".txt",
                           configReceiver)
        topology = nx.Graph()
        self.configReceiver = configReceiver
        self.configSender = configSender
        createGraph(topology, self.configSender, self.configReceiver, randomizeLinkCapacities=False)
        self.topology = topology

    def test_configurationRunShortestLantencyPaths(self):
        results = configurationRunShortestLatencyPaths(self.topology, self.configSender, self.configReceiver)
        route1 = Route(bottleneck=1.0, destination='2/8', latency=math.inf, route=['r3', 'r5'], traffic=4.0)
        route2 = Route(bottleneck=3.0, destination='1/8', latency=math.inf, route=['r1', 'r2'], traffic=4.0)
        self.assertIn(route1, results)
        self.assertIn(route2, results)

    def test_deleteDemand(self):
        route1 = Route(bottleneck=4.0, destination='2/8', latency=3.0, route=['r3', 'r5'], traffic=4.0)
        tempConfigSender = createDeepCopyConfigSender(self.configSender)
        tempConfigSender.traffic['2/8'] = 0.0
        self.assertTrue(deleteDemand([route1], tempConfigSender, False))

    def test_configurationRunShortestPathsCommunication(self):
        results_band = configurationRunShortestPathsCommunication(self.topology, self.configSender, self.configReceiver,
                                                                  'bandwidth')
        route1 = Route(bottleneck=2.5, destination='2/8', latency=7.0, route=['r3', 'r2', 'r4', 'r5'], traffic=4.0)
        route2 = Route(bottleneck=2.5, destination='1/8', latency=3.0, route=['r3', 'r2'], traffic=4.0)
        self.assertIn(route1, results_band)
        self.assertIn(route2, results_band)

        results_lat = configurationRunShortestPathsCommunication(self.topology, self.configSender, self.configReceiver,
                                                                  'latency')
        route1 = Route(bottleneck=1.0, destination='2/8', latency=5.0, route=['r3', 'r5'], traffic=4.0)
        route2 = Route(bottleneck=3.0, destination='1/8', latency=2.0, route=['r1', 'r2'], traffic=4.0)
        self.assertIn(route1, results_lat)
        self.assertIn(route2, results_lat)

    def test_optimizerCommunicationAdvanced(self):
        results_band = optimizerCommunicationAdvanced(self.topology, self.configSender, self.configReceiver,
                                                 attribute_optimizer='bandwidth')
        route1 = Route(bottleneck=2.5, destination='2/8', latency=7.0, route=['r3', 'r2', 'r4', 'r5'], traffic=4.0)
        route2 = Route(bottleneck=2.5, destination='1/8', latency=3.0, route=['r3', 'r2'], traffic=4.0)
        route3 = Route(bottleneck=1.0, destination='2/8', latency=5.0, route=['r3', 'r5'], traffic=1.5)
        route4 = Route(bottleneck=1.5, destination='1/8', latency=2.0, route=['r1', 'r2'], traffic=1.5)
        route5 = Route(bottleneck=0.5, destination='2/8', latency=6.0, route=['r1', 'r2', 'r4', 'r5'], traffic=0.5)
        self.assertIn(route1, results_band)
        self.assertIn(route2, results_band)
        self.assertIn(route3, results_band)
        self.assertIn(route4, results_band)
        self.assertIn(route5, results_band)

        results_lat = optimizerCommunicationAdvanced(self.topology, self.configSender, self.configReceiver,
                                                     attribute_optimizer='latency')
        route2 = Route(bottleneck=1.0, destination='2/8', latency=5.0, route=['r3', 'r5'], traffic=4.0)
        route3 = Route(bottleneck=3.0, destination='1/8', latency=2.0, route=['r1', 'r2'], traffic=4.0)
        route1 = Route(bottleneck=3.0, destination='2/8', latency=7.0, route=['r3', 'r2', 'r4', 'r5'], traffic=3.0)
        route4 = Route(bottleneck=1.0, destination='1/8', latency=3.0, route=['r3', 'r2'], traffic=1.0)
        self.assertIn(route1, results_lat)
        self.assertIn(route2, results_lat)
        self.assertIn(route3, results_lat)
        self.assertIn(route4, results_lat)


if __name__ == '__main__':
    unittest.main()
