import copy
import math
import sys
from dataclasses import dataclass

from NegotiationProtocol.controllers import createGraph, findRouterForDestination, getShortestRouteDijkstra, \
    findBottleneck, Route, findSharedLinks, findDelayRoute, createDeepCopyConfigSender, \
    createDeepCopyConfigReceiver


@dataclass
class OptionsForCP:
    capacity: float
    delay: float
    route: []
    destinations: []


def computeOptionsForCP(sharedLinks, routes, topology):
    tempTopology = topology.copy()
    optionsForCP = []

    for route in routes:
        option = OptionsForCP(route.bottleneck, route.latency, route.route, [route.destination])
        optionsForCP.append(option)

        for sharedLink in sharedLinks:
            if route in sharedLink.routesSharingLink:
                print("For destination {}, link {} is shared by {} routes".format(route.destination, sharedLink.link,
                                                                                  len(sharedLink.routesSharingLink)))
                print("You can allow all capacity for link {} to {}".format(sharedLink.link, route.destination))
                print("Or you can split between {} destinations".format(len(sharedLink.routesSharingLink)))
                newTopologyAfterSplitting = assignCapacityToLinkAfterSplitting(sharedLink.link,
                                                                               len(sharedLink.routesSharingLink),
                                                                               tempTopology)
                newBottleneckAfterSplitting = findBottleneck(newTopologyAfterSplitting, list(
                    filter(lambda el: (el.destination == route.destination), routes))[0].route)
                trafficRequestedToBeSent = min(route.traffic, newBottleneckAfterSplitting)
                print("If you split, the maximum you will be able to send for destination {} is {} ".format(
                    route.destination,
                    trafficRequestedToBeSent))
                delaySharedLinks = findDelayMultipleRoutes(sharedLink.routesSharingLink, tempTopology)
                destinations = getDestinationsMultipleRoutes(routes, sharedLink.routesSharingLink)
                option = OptionsForCP(trafficRequestedToBeSent, delaySharedLinks, sharedLink.routesSharingLink,
                                      destinations)
                if option not in optionsForCP:
                    optionsForCP.append(option)
                tempTopology = topology.copy()

    return optionsForCP


def selectOption(options):
    minimumLatency = sys.maxsize
    optionBestLatency = OptionsForCP(0.0, 0.0, [], [])
    for option in options:
        if option.delay < minimumLatency or option.delay == minimumLatency and len(option.destinations) > len(
                optionBestLatency.destinations):
            optionBestLatency = option
            minimumLatency = option.delay
    return optionBestLatency


def findDelayMultipleRoutes(routes, topology):
    delay = 0.0
    for element in routes:
        delay = max(findDelayRoute(element.route, topology), delay)
    return delay


def getDestinationsMultipleRoutes(routes, routesSharingLink):
    destinations = []
    for route in routes:
        if route.route in routesSharingLink:
            destinations.append(route.destination)
    return destinations


def assignCapacityToLinkAfterSplitting(link, noDestinationsSharingLink, topology):
    routers = link.split('->')
    tempTopology = topology
    if tempTopology.has_edge(routers[0], routers[1]):
        tempTopology[routers[0]][routers[1]]['bandwidth'] = tempTopology[routers[0]][routers[1]][
                                                                'bandwidth'] / noDestinationsSharingLink
    return tempTopology


def configurationRunBandwidth(topology, configSender, configReceiver, randomizeLinkCapacities=False,
                              noEgressRoutersHosts=None, noIngressRoutersISP=None, maxCap=0):
    print("Results for bandwidth optimization: ")
    routes = []
    createGraph(topology, configSender, configReceiver, randomizeLinkCapacities, noEgressRoutersHosts,
                noIngressRoutersISP, maxCap)

    tempTopology = topology.copy()

    configSenderTemp = createDeepCopyConfigSender(configSender)
    configReceiverTemp = createDeepCopyConfigReceiver(configReceiver)

    results_bandwidth_opt = []

    for destination in configSenderTemp.destination_hosts:
        router_destination = findRouterForDestination(configReceiverTemp, destination)
        shortest_path = getShortestRouteDijkstra(configReceiverTemp, topology, router_destination, 'bandwidth')
        traffic = configSenderTemp.traffic[destination]
        bottleneck = findBottleneck(tempTopology, shortest_path[1])
        latency = findDelayRoute(shortest_path[1], tempTopology)
        route = Route(route=shortest_path[1], bottleneck=min(traffic, bottleneck), traffic=traffic, latency=latency,
                      destination=destination)
        if traffic > bottleneck:
            route.latency = math.inf
        results_bandwidth_opt.append(route)
        routes.append(route)

    sharedLinks = findSharedLinks(routes)
    for sharedLink in sharedLinks:
        routers = sharedLink.link.split('->')
        linkCapacity = tempTopology[routers[0]][routers[1]]['bandwidth']
        traffic = 0.0
        for route in sharedLink.routesSharingLink:
            traffic += route.traffic
            if traffic > linkCapacity:
                for route in sharedLink.routesSharingLink:
                    list(filter(lambda el: (el.destination == route.destination), results_bandwidth_opt))[
                        0].latency = math.inf
                    list(filter(lambda el: (el.destination == route.destination), results_bandwidth_opt))[0].bottleneck = \
                        min(min(route.traffic, route.bottleneck), linkCapacity / len(sharedLink.routesSharingLink))

    for route in results_bandwidth_opt:
        print(
            "For destination {} we send {} GB with {} delay".format(route.destination, route.bottleneck, route.latency))
    print("")

    return results_bandwidth_opt


def advancedNegotiation(topology, configSender, configReceiver, attribute_negotiation, randomizeLinkCapacities=False,
                        noEgressRoutersHosts=None, noIngressRoutersISP=None, maxCap=0):
    createGraph(topology, configSender, configReceiver, randomizeLinkCapacities, noEgressRoutersHosts,
                noIngressRoutersISP, maxCap)
    tempTopology = topology.copy()

    trafficDemands = copy.deepcopy(configSender.traffic)
    configSenderTemp = createDeepCopyConfigSender(configSender)
    results_negotiation = []

    paths_available_for_demands = {}
    for destination in configSenderTemp.destination_hosts:
        paths_available_for_demands[destination] = True

    found_paths = True
    while len(trafficDemands) > 0 and found_paths == True:
        demandSatisfied = False
        while demandSatisfied == False and found_paths == True:
            no_paths, new_result = configurationRunNegotiationProtocol(tempTopology, configSenderTemp, configReceiver,
                                                                       attribute_negotiation)
            if no_paths == -1:
                paths_available_for_demands[new_result] = False
                del trafficDemands[new_result]
                del configSenderTemp.destination_hosts[configSenderTemp.destination_hosts.index(new_result)]
                found_paths = False
                for i, elem in enumerate(paths_available_for_demands):
                    if paths_available_for_demands[elem]:
                        found_paths = True
                        break
                print("No more paths available to meet the rest of the demands")
                continue
            try:
                results_negotiation.append(new_result[0])
            except:
                found_paths = False
                break
            for result in new_result:
                configSenderTemp.traffic[result.destination] -= min(result.traffic, result.bottleneck)
                trafficDemands[result.destination] -= min(result.traffic, result.bottleneck)
                for i, j in zip(result.route, result.route[1:]):
                    tempTopology[i][j]['bandwidth'] -= min(result.traffic, result.bottleneck)
                    if tempTopology[i][j]['bandwidth'] == 0:
                        tempTopology.remove_edge(i, j)
            destinations = []
            for route in new_result:
                destinations.append(route.destination)
            for destination in destinations:
                if trafficDemands[destination] == 0:
                    del trafficDemands[destination]
                    del configSenderTemp.destination_hosts[configSenderTemp.destination_hosts.index(destination)]
                    demandSatisfied = True

    return results_negotiation


def configurationRunNegotiationProtocol(topology, configSender, configReceiver, attribute_negotiation):
    print("Results for negotiation: ")
    routes = []

    tempTopology = topology.copy()
    totalTraffic = 0.0

    for destination in configSender.destination_hosts:
        router_destination = findRouterForDestination(configReceiver, destination)
        try:
            if attribute_negotiation == 'bandwidth':
                shortest_path = getShortestRouteDijkstra(configReceiver, topology, router_destination, 'bandwidth')
            else:
                shortest_path = getShortestRouteDijkstra(configReceiver, tempTopology, router_destination, 'latency')

        except:
            return -1, destination
        traffic = configSender.traffic[destination]
        bottleneck = findBottleneck(tempTopology, shortest_path[1])
        latency = findDelayRoute(shortest_path[1], tempTopology)
        route = Route(route=shortest_path[1], bottleneck=bottleneck, traffic=traffic, latency=latency,
                      destination=destination)
        route = route
        routes.append(route)

    sharedLinks = findSharedLinks(routes)
    print("The shared links are: {}".format(sharedLinks))
    optionsCP = computeOptionsForCP(sharedLinks, routes, tempTopology)
    option = selectOption(optionsCP)

    for destination in option.destinations:
        totalTraffic += configSender.traffic[destination]
    if option.capacity > totalTraffic:
        option.capacity = totalTraffic

    results_obtained = []

    print(
        "The option selected is to send {} GB of traffic per flow along routes {} for destinations {}, so a total of "
        "{} GB with delay {} ms"
            .format(option.capacity, option.route, option.destinations, option.capacity * len(option.destinations),
                    option.delay))
    for opt in option.destinations:
        route = Route(route=option.route, bottleneck=option.capacity, traffic=option.capacity,
                      latency=option.delay,
                      destination=opt)
        results_obtained.append(route)
    print("Congestion is not created")
    print("")

    return 1, results_obtained
