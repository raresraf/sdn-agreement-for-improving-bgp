import copy
import networkx as nx
import sys
from dataclasses import dataclass

from NegotiationProtocol.CP import ConfigSender
from NegotiationProtocol.ISP import ConfigReceiver
from NegotiationProtocol.randomizers import reduceLinksCapacity, randomizeIngressRouters, randomizeEgressRouters, \
    randomizeTraffic

results_file = None


def setRoutingResultsFile(results_file_name):
    global results_file
    results_file = open(results_file_name, "w")
    sys.stdout = results_file


def createGraph(topology, configSender, configReceiver, randomizeLinkCapacities=False, noEgressRoutersHosts=None,
                noIngressRoutersISP=None, maxCapacity=0):
    for node in configSender.egress_points:
        if node not in topology.nodes:
            topology.add_node(node)
    for node in configReceiver.routers:
        if node not in topology.nodes:
            topology.add_node(node)
    for link in configReceiver.links:
        topology.add_edge(link.router1, link.router2, bandwidth=link.capacity, latency=link.latency)
        topology.add_edge(link.router2, link.router1, bandwidth=link.capacity, latency=link.latency)
    if randomizeLinkCapacities:
        reduceLinksCapacity(topology)
    if noIngressRoutersISP is not None:
        randomizeIngressRouters(topology, configSender, configReceiver, noIngressRoutersISP)
    if noEgressRoutersHosts is not None:
        randomizeEgressRouters(topology, configSender, configReceiver, noEgressRoutersHosts)
    if maxCapacity is not 0:
        randomizeTraffic(topology, configSender, maxCapacity)


def getShortestRouteDijkstra(configReceiver, topology, destination, optimization):
    if optimization == 'bandwidth':
        shortest_paths = nx.multi_source_dijkstra(G=topology, sources=configReceiver.ingress_points, target=destination,
                                                  weight=lambda u, v, d: 1 / topology.edges[u, v][optimization])
    else:
        shortest_paths = nx.multi_source_dijkstra(G=topology, sources=configReceiver.ingress_points, target=destination,
                                                  weight=lambda u, v, d: topology.edges[u, v][optimization])
    print("Shortest path for destination {} is {}".format(destination, shortest_paths))
    return shortest_paths


def findRouterForDestination(configReceiver, host):
    for dest in configReceiver.hosts:
        if dest.host == host:
            return dest.router
    return 'Not a host'


def findBottleneck(topology, path):
    bottleneck = sys.maxsize
    for link in range(len(path) - 1):
        if topology.has_edge(path[link], path[link + 1]):
            linkCapacity = topology[path[link]][path[link + 1]]['bandwidth']
            if linkCapacity < bottleneck:
                bottleneck = linkCapacity
    return bottleneck


def findSharedLinksBetweenTwoRoutes(route1, route2):
    sharedLinks = []
    for link1 in range(len(route1.route)):
        for link2 in range(len(route2.route)):
            if (route1.route[link1] == route2.route[link2] and link1 < len(route1.route) - 1 and link2 < len(
                    route2.route) - 1
                and route1.route[link1 + 1] == route2.route[link2 + 1]) or (link2 < len(route2.route) - 1 and
                                                                            link1 < len(route1.route) - 1 and
                                                                            route1.route[link1] == route2.route[
                                                                                link2 + 1] and route1.route[link1 + 1]
                                                                            == route2.route[link2 + 1]) or (
                    link2 < len(route2.route) - 1 and link1 < len(route1.route) - 1 and
                    route1.route[link1 + 1] == route2.route[link2] and route1.route[link1] == route2.route[link2 + 1]):
                if sharedLinks.__contains__(route1.route[link1] + "->" + route1.route[link1 + 1]) == False and \
                        sharedLinks.__contains__(route1.route[link1 + 1] + "->" + route1.route[link1]) == False:
                    sharedLinks.append(route1.route[link1] + "->" + route1.route[link1 + 1])
    return sharedLinks


@dataclass
class SharedLink:
    link: str
    routesSharingLink: []


@dataclass()
class Route:
    route: []
    bottleneck: float
    traffic: float
    latency: float
    destination: str


def findSharedLinks(listOfRoutes):
    sharedLinks = []
    for i in range(len(listOfRoutes)):
        for j in range(i + 1, len(listOfRoutes)):
            sharedLinks2Routes = findSharedLinksBetweenTwoRoutes(listOfRoutes[i], listOfRoutes[j])
            for link in sharedLinks2Routes:
                sharedLinks.append(SharedLink(link, [listOfRoutes[i], listOfRoutes[j]]))
    return sharedLinks


def getTrafficForDestination(destination, configCP):
    for traffic in configCP.traffic:
        if traffic.destination == destination:
            return traffic.traffic


def findDelayRoute(route, topology):
    delay = 0.0
    for link in range(len(route) - 1):
        delay += topology[route[link]][route[link + 1]]['latency']
    return delay


def getCapacityForDestinationHost(configSender, destination):
    for data in configSender.traffic:
        if data.destination == destination:
            return data.traffic
    return -1


def createDeepCopyConfigSender(oldObject):
    new_file = oldObject.file
    new_reading = oldObject.reading
    new_traffic = copy.deepcopy(oldObject.traffic)
    new_destination_hosts = copy.deepcopy(oldObject.destination_hosts)
    new_egress_points = copy.deepcopy(oldObject.egress_points)
    return ConfigSender(new_file, new_reading, new_traffic, new_destination_hosts, new_egress_points)


def createDeepCopyConfigReceiver(oldObject):
    new_file = oldObject.file
    new_reading = oldObject.reading
    new_routers = oldObject.routers
    new_ingress_points = copy.deepcopy(oldObject.ingress_points)
    new_hosts = copy.deepcopy(oldObject.hosts)
    new_links = copy.deepcopy(oldObject.links)
    return ConfigReceiver(new_file, new_reading, new_routers, new_ingress_points, new_hosts, new_links)
