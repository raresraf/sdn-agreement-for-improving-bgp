import math
import random

log_file = None


def setLogFile(log_file_name):
    global log_file
    log_file = log_file_name


def randomizeIngressRouters(topology, configSender, configReceiver, percOfIngressRoutersISPToChoose):
    noNodes = len(topology.nodes)
    noIngressRouters = math.ceil(percOfIngressRoutersISPToChoose * noNodes / 100)
    newIngressRouters = random.sample(topology.nodes, noIngressRouters)
    if noIngressRouters > 0:
        configSender.egress_points.clear()
        configSender.egress_points = newIngressRouters
        configReceiver.ingress_points.clear()
        configReceiver.ingress_points = newIngressRouters
    log_file.write("New Ingress Routers ISP: {} \n".format(configReceiver.ingress_points))


def findMaximumLinkCapacity(topology):
    maxLinkCap = 0.0
    for link in topology.edges:
        maxLinkCap = max(maxLinkCap, topology[link[0]][link[1]]['bandwidth'])
    return maxLinkCap


def randomizeEgressRouters(topology, configSender, configReceiver, percOfEgressRoutersHostsToChoose):
    noNodes = len(topology.nodes)
    noEgressRouters = math.ceil(percOfEgressRoutersHostsToChoose * noNodes / 100)
    newEgressRouters = []
    if noEgressRouters > 0:
        while len(newEgressRouters) < noEgressRouters:
            newEgressPoint = random.sample(topology.nodes, 1)
            if newEgressPoint not in configSender.egress_points and newEgressPoint not in newEgressRouters and \
                    newEgressPoint not in configReceiver.ingress_points:
                newEgressRouters.append(newEgressPoint)
        configSender.destination_hosts.clear()
        configReceiver.hosts.clear()
        for i, point in enumerate(newEgressRouters):
            configReceiver.hosts[str(i) + "/8"] = point[0]
            configSender.destination_hosts.append(str(i) + "/8")
    log_file.write("New Egress Routers Hosts: {} \n".format(configReceiver.hosts))


def assignTraffic(topology, configSender, traffic):
    traffic = traffic * findMaximumLinkCapacity(topology)
    noEgressRouters = len(configSender.destination_hosts)
    trafficPerEgress = traffic/noEgressRouters
    configSender.traffic.clear()
    for egress in configSender.destination_hosts:
        configSender.traffic[egress] = trafficPerEgress
    log_file.write("Attempted traffic to send: {} \n".format(configSender.traffic))


def randomizeTraffic(topology, configSender, traffic):
    traffic = traffic * findMaximumLinkCapacity(topology)
    trafficLeft = traffic
    configSender.traffic.clear()
    for i in range(0, len(configSender.destination_hosts) - 1):
        rand = random.randrange(0, 100)
        newTraffic = rand * trafficLeft / 100
        configSender.traffic[configSender.destination_hosts[i]] = newTraffic
        trafficLeft -= newTraffic
    configSender.traffic[configSender.destination_hosts[len(configSender.destination_hosts)-1]] = trafficLeft
    log_file.write("New traffic: {} \n".format(configSender.traffic))

