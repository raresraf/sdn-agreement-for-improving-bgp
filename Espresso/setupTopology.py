import math

from Espresso.espresso import Espresso, findBestDelayPathForPrefix, findWorstDelayPath, moveTraffic, removeTraffic
from Espresso.topology import Topology

log_file = None

PERC_PATHS_TO_MOVE_TRAFFIC = 1/5
PERC_TRAFFIC_TO_MOVE = 10/100
NO_ROUNDS = 40


def setLogFile(logFile):
    global log_file
    log_file = logFile


def buildTopology(cp, isp):
    topology = Topology()
    topology.buildTopology(cp, isp)
    return topology


def startOptimization(topology, cp, isp):

    tempTopology = topology.copy()
    espresso = Espresso(topology, cp)

    # Assign traffic equally to all egresses for first round
    espresso.start()
    trafficDemands = espresso.getTrafficDemands()
    isp.setTopology(tempTopology)
    isp.reduceLinksCapacity()
    isp.calculateAllPaths(espresso.egressRouters)
    isp.sendTraffic(trafficDemands)
    delays = isp.getRoutesDelay()
    isp.restoreLinksCapacity(topology.copy())

    PATHS_TO_MOVE_TRAFFIC = math.ceil(PERC_PATHS_TO_MOVE_TRAFFIC * len(isp.hosts) * espresso.noEgressPoints)

    delaysAllRounds = []
    # Run rounds
    for _ in range(0, NO_ROUNDS):
        alreadyUsedEgresses = []
        isp.reduceLinksCapacity()
        isp.calculateAllPaths(espresso.egressRouters)
        for _ in range(0, PATHS_TO_MOVE_TRAFFIC):
            worstDelayResult = findWorstDelayPath(alreadyUsedEgresses, delays)
            bestDelayIngressAlternative = findBestDelayPathForPrefix(delays, worstDelayResult.source, worstDelayResult.target)
            if bestDelayIngressAlternative == -1:
                break
            trafficToMove = worstDelayResult.traffic * PERC_TRAFFIC_TO_MOVE
            moveTraffic(trafficDemands, bestDelayIngressAlternative, worstDelayResult.target, trafficToMove)
            removeTraffic(trafficDemands, worstDelayResult.source, worstDelayResult.target, trafficToMove)
        isp.deleteOldDelays()
        isp.sendTraffic(trafficDemands)
        delays = isp.getRoutesDelay()
        delaysAllRounds.append(delays.copy())
        isp.restoreLinksCapacity(topology.copy())

    return delaysAllRounds
