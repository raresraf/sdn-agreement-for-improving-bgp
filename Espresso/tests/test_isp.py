import math
import unittest
from pathlib import Path

from Espresso import randomizers, setupTopology, espresso
from Espresso.cp import CP
from Espresso.egressRouter import EgressRouter
from Espresso.espresso import Espresso
from Espresso.isp import ISP
from Espresso.isp import setLogFile as ispSetLogFile
from Espresso.route import Route
from Espresso.topology import Topology

path2Up = Path(__file__).parents[2]

topology_to_test = "_topReport"
log_file = "../results/log" + topology_to_test + ".txt"


def have_same_contents_obj(obj1, obj2):
    if obj1.delay != obj2.delay or obj1.route != obj2.route or obj1.source != obj2.source or obj1.target != obj2.target \
            or obj1.traffic != obj2.traffic:
        return False
    return True


class MyTestCase(unittest.TestCase):
    def setUp(self):
        global log_file
        try:
            log_file = open(log_file, "w")
        except:
            log_file = log_file
        setupTopology.setLogFile(log_file)
        espresso.setLogFile(log_file)
        randomizers.setLogFile(log_file)
        ispSetLogFile(log_file)
        filename_cp = "{}/configurations/configSender".format(path2Up) + topology_to_test + ".txt"
        filename_isp = "{}/configurations/configReceiver".format(path2Up) + topology_to_test + ".txt"
        self.cp = CP(filename_cp)
        self.isp = ISP(filename_isp)
        self.topology = Topology()
        self.topology.buildTopology(self.cp, self.isp)
        self.isp.setTopology(self.topology)

    def test_getShortestRoute(self):
        self.assertEqual(self.isp.getShortestRoute('r1', 'r2'), ['r1', 'r2'])
        self.assertEqual(self.isp.getShortestRoute('r3', 'r5'), ['r3', 'r2', 'r4', 'r5'])
        self.assertEqual(self.isp.getShortestRoute('r3', 'r6'), -1)

    def test_calculateAllPaths(self):
        espresso = Espresso(self.topology, self.cp)
        espresso.start()
        tempTopology = self.topology.copy()
        self.isp.setTopology(tempTopology)
        self.isp.calculateAllPaths(espresso.egressRouters)
        router118 = Route(route=['r1', 'r2'], source='r1', target='1/8')
        router128 = Route(route=['r1', 'r2', 'r4', 'r5'], source='r1', target='2/8')
        router318 = Route(route=['r3', 'r2'], source='r3', target='1/8')
        router328 = Route(route=['r3', 'r2', 'r4', 'r5'], source='r3', target='2/8')
        self.assertTrue(have_same_contents_obj(self.isp.routes[('r1', self.isp.hosts['1/8'])], router118), True)
        self.assertTrue(have_same_contents_obj(self.isp.routes[('r1', self.isp.hosts['2/8'])], router128), True)
        self.assertTrue(have_same_contents_obj(self.isp.routes[('r3', self.isp.hosts['1/8'])], router318), True)
        self.assertTrue(have_same_contents_obj(self.isp.routes[('r3', self.isp.hosts['2/8'])], router328), True)

    def test_receiveTraffic(self):
        self.setUp()
        espresso = Espresso(self.topology, self.cp)
        espresso.start()
        tempTopology = self.topology.copy()
        self.isp.setTopology(tempTopology)
        self.isp.calculateAllPaths(espresso.egressRouters)
        newEgressRouter = EgressRouter('r1')
        newEgressRouter.traffic['2/8'] = 2.0
        traffic = [newEgressRouter]
        self.isp.receiveTraffic(traffic)
        self.assertEqual(self.isp.topology['r1']['r2']['bandwidth'], 1.0)
        self.assertEqual(self.isp.topology['r2']['r4']['bandwidth'], 2.0)
        self.assertEqual(self.isp.topology['r4']['r5']['bandwidth'], 3.0)
        del newEgressRouter.traffic['2/8']
        newEgressRouter.traffic['1/8'] = 2.0
        traffic = [newEgressRouter]
        self.isp.receiveTraffic(traffic)
        self.assertEqual(self.isp.topology['r1']['r2']['bandwidth'], -1.0)
        congestedRoute = list(filter(lambda el: (el.target == '1/8' and el.source == 'r1'), self.isp.routesDelays))[0]
        self.assertEqual(congestedRoute.delay, math.inf)

    def test_decreaseBandwidth(self):
        self.setUp()
        espresso = Espresso(self.topology, self.cp)
        espresso.start()
        tempTopology = self.topology.copy()
        self.isp.setTopology(tempTopology)
        self.isp.calculateAllPaths(espresso.egressRouters)
        newEgressRouter = EgressRouter('r1')
        newEgressRouter.traffic['2/8'] = 2.0
        traffic = [newEgressRouter]
        self.isp.decreaseBandwidth(traffic)
        self.assertEqual(self.isp.topology['r1']['r2']['bandwidth'], 1.0)
        self.assertEqual(self.isp.topology['r2']['r4']['bandwidth'], 2.0)
        self.assertEqual(self.isp.topology['r4']['r5']['bandwidth'], 3.0)
        newEgressRouter = EgressRouter('r1')
        newEgressRouter.traffic['1/8'] = 0.0
        traffic = [newEgressRouter]
        self.isp.decreaseBandwidth(traffic)
        self.assertEqual(self.isp.topology['r1']['r2']['bandwidth'], 1.0)

    def test_pathCreatesCongestion(self):
        self.setUp()
        self.isp.topology['r3']['r2']['bandwidth'] = -1
        self.assertTrue(self.isp.pathCreatesCongestion(['r3', 'r2', 'r4', 'r5']))
        self.assertFalse(self.isp.pathCreatesCongestion(['r1', 'r2']))

    def test_decreaseTrafficForPath(self):
        self.setUp()
        self.isp.decreaseTrafficForPath(['r3', 'r2', 'r4', 'r5'], 2.0)
        self.assertEqual(self.isp.topology['r3']['r2']['bandwidth'], 3.0)
        self.assertEqual(self.isp.topology['r2']['r4']['bandwidth'], 2.0)
        self.assertEqual(self.isp.topology['r4']['r5']['bandwidth'], 3.0)
        self.isp.decreaseTrafficForPath(['r3', 'r5'], 1.0)
        self.assertEqual(self.isp.topology['r3']['r5']['bandwidth'], 0.0)

    def test_getDelayForPath(self):
        self.assertEqual(self.isp.getDelayForPath(['r3', 'r2', 'r4', 'r5']), 7.0)
        self.assertEqual(self.isp.getDelayForPath(['r3']), 0.0)


if __name__ == '__main__':
    unittest.main()
