import math
import unittest
from pathlib import Path

from Espresso import randomizers, setupTopology, espresso
from Espresso.cp import CP
from Espresso.espresso import findWorstDelayPath, findBestDelayPathForPrefix, Espresso, moveTraffic, removeTraffic
from Espresso.isp import ISP
from Espresso.isp import setLogFile as ispSetLogFile
from Espresso.topology import Topology

path2Up = Path(__file__).parents[2]

topology_to_test = "_topReport"
log_file = "../results/log" + topology_to_test + ".txt"


class TestEspresso(unittest.TestCase):
    def setUp(self):
        global log_file
        try:
            log_file = open(log_file, "w")
        except:
            log_file = log_file
        setupTopology.setLogFile(log_file)
        espresso.setLogFile(log_file)
        randomizers.setLogFile(log_file)
        ispSetLogFile(log_file)
        filename_cp = "{}/configurations/configSender".format(path2Up) + topology_to_test + ".txt"
        filename_isp = "{}/configurations/configReceiver".format(path2Up) + topology_to_test + ".txt"
        self.cp = CP(filename_cp)
        self.isp = ISP(filename_isp)
        self.topology = Topology()
        self.topology.buildTopology(self.cp, self.isp)

    def test_findWorstDelayPath(self):
        espresso = Espresso(self.topology, self.cp)
        espresso.start()
        trafficDemands = espresso.getTrafficDemands()
        tempTopology = self.topology.copy()
        self.isp.setTopology(tempTopology)
        self.isp.reduceLinksCapacity()
        self.isp.calculateAllPaths(espresso.egressRouters)
        self.isp.sendTraffic(trafficDemands)
        delays = self.isp.getRoutesDelay()
        max = 0.0
        worstRes = None
        for delay in delays:
            if delay.delay > max:
                worstRes = delay
                max = delay.delay
        self.assertEqual(findWorstDelayPath([], delays), worstRes)

    def test_findBestDelaypath(self):
        espresso = Espresso(self.topology, self.cp)
        espresso.start()
        trafficDemands = espresso.getTrafficDemands()
        tempTopology = self.topology.copy()
        self.isp.setTopology(tempTopology)
        self.isp.reduceLinksCapacity()
        self.isp.calculateAllPaths(espresso.egressRouters)
        self.isp.sendTraffic(trafficDemands)
        delays = self.isp.getRoutesDelay()
        if list(filter(lambda el: (el.target == '1/8' and el.source == 'r3'), delays))[0].delay != math.inf:
            altPathr118 = 'r3'
        else:
            altPathr118 = -1
        if list(filter(lambda el: (el.target == '1/8' and el.source == 'r1'), delays))[0].delay != math.inf:
            altPathr318 = 'r1'
        else:
            altPathr318 = -1
        if list(filter(lambda el: (el.target == '2/8' and el.source == 'r3'), delays))[0].delay != math.inf:
            altPathr128 = 'r3'
        else:
            altPathr128 = -1
        if list(filter(lambda el: (el.target == '2/8' and el.source == 'r1'), delays))[0].delay != math.inf:
            altPathr328 = 'r1'
        else:
            altPathr328 = -1
        self.assertEqual(findBestDelayPathForPrefix(delays, 'r1', '1/8'), altPathr118)
        self.assertEqual(findBestDelayPathForPrefix(delays, 'r3', '1/8'), altPathr318)
        self.assertEqual(findBestDelayPathForPrefix(delays, 'r1', '2/8'), altPathr128)
        self.assertEqual(findBestDelayPathForPrefix(delays, 'r3', '2/8'), altPathr328)

    def test_moveTraffic(self):
        espresso = Espresso(self.topology, self.cp)
        espresso.start()
        trafficDemands = espresso.getTrafficDemands()
        tempTopology = self.topology.copy()
        self.isp.setTopology(tempTopology)
        self.isp.reduceLinksCapacity()
        self.isp.calculateAllPaths(espresso.egressRouters)
        self.isp.sendTraffic(trafficDemands)
        moveTraffic(trafficDemands, 'r1', '1/8', 10)
        self.assertEqual(trafficDemands[0].traffic['1/8'], 12.0)
        moveTraffic(trafficDemands, 'r3', '2/8', 0)
        self.assertEqual(trafficDemands[1].traffic['2/8'], 2.0)
        self.assertRaises(KeyError, moveTraffic(trafficDemands, 'r3', '3/8', 0))

    def test_removeTraffic(self):
        espresso = Espresso(self.topology, self.cp)
        espresso.start()
        trafficDemands = espresso.getTrafficDemands()
        tempTopology = self.topology.copy()
        self.isp.setTopology(tempTopology)
        self.isp.reduceLinksCapacity()
        self.isp.calculateAllPaths(espresso.egressRouters)
        self.isp.sendTraffic(trafficDemands)
        removeTraffic(trafficDemands, 'r1', '1/8', 1)
        self.assertEqual(trafficDemands[0].traffic['1/8'], 1.0)
        removeTraffic(trafficDemands, 'r3', '2/8', 0)
        self.assertEqual(trafficDemands[1].traffic['2/8'], 2.0)
        self.assertRaises(KeyError, removeTraffic(trafficDemands, 'r3', '3/8', 0))


if __name__ == '__main__':
    unittest.main()
