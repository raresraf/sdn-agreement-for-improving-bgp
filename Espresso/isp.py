import math
import networkx as nx
import random

from Espresso.link import Link
from Espresso.route import Route

log_file = None

TOTAL_PERC_LINKS_TO_MODIFY = 100
FIRST_PERC_LINKS_TO_MODIFY = 25
LOWER_FIRST_PERC = 60
UPPER_FIRST_PERC = 80
SECOND_PERC_LINKS_TO_MODIFY = 10
LOWER_SECOND_PERC = 40
UPPER_SECOND_PERC = 59
THIRD_PERC_LINKS_TO_MODIFY = 20
LOWER_THIRD_PERC = 20
UPPER_THIRD_PERC = 39
FOURTH_PERC_LINKS_TO_MODIFY = 45
LOWER_FOURTH_PERC = 0
UPPER_FOURTH_PERS = 19


def setLogFile(logFile):
    global log_file
    log_file = logFile


class ISP:

    def __init__(self, filename):
        self.topology = None
        self.filename = filename
        self.reading = None
        self.routers = []
        self.ingress_points = []
        self.hosts = {}
        self.links = []
        self.readConfigSender()
        self.routes = {}
        self.newTrafficReceived = []
        self.routesDelays = []

    def readConfigSender(self):
        file = open(self.filename)
        row = file.readlines()
        for line in row:
            if line.find("Topology") > -1:
                self.reading = "Topology"
                continue
            if line.find("Ingress") > -1:
                self.reading = "Ingress"
                continue
            if line.find("Hosts") > -1:
                self.reading = "Hosts"
                continue
            if line.find("Capacities") > -1:
                self.reading = "Capacities"
                continue
            if self.reading == "Topology":
                self.routers.append(line.rstrip())
            if self.reading == "Capacities":
                parts = line.rstrip()
                parts = parts.split(':')
                tr = parts[0].split("-")
                cl = parts[1].split("/")
                self.links.append(Link(tr[0], tr[1], float(cl[0]), float(cl[1])))
            if self.reading == "Ingress":
                self.ingress_points.append(line.rstrip())
            if self.reading == "Hosts":
                parts = line.rstrip()
                parts = parts.split('-')
                self.hosts[parts[0]] = parts[1]

    def setTopology(self, topology):
        self.topology = topology.copy()

    def getShortestRoute(self, start, target):
        try:
            shortestRoute = nx.shortest_path(G=self.topology, source=start, target=target,
                                             weight=lambda u, v, d: 1 / self.topology.edges[u, v]['bandwidth'])
        except nx.NetworkXNoPath:
            return -1
        print("Shortest path for destination {} is {}".format(target, shortestRoute))
        return shortestRoute

    def calculateAllPaths(self, ingressRouters):
        for ingress in ingressRouters:
            for host in ingress.traffic:
                shortestRoute = self.getShortestRoute(ingress.router, self.hosts[host])
                if shortestRoute == -1:
                    log_file.write("No route from {} to {} \n".format(ingress.router, self.hosts[host]))
                    continue
                newRoute = Route(ingress.router, host, shortestRoute)
                self.routes[(ingress.router, self.hosts[host])] = newRoute

    def sendTraffic(self, traffic):
        self.newTrafficReceived = traffic
        self.receiveTraffic(traffic)

    def receiveTraffic(self, traffic):
        self.decreaseBandwidth(traffic)
        for ingress in traffic:
            for demand in ingress.traffic:
                target = self.hosts[demand]
                path = self.routes[(ingress.router, target)]
                path.setTraffic(ingress.traffic[demand])
                if self.pathCreatesCongestion(path.route):
                    path.setDelay(math.inf)
                    self.routesDelays.append(path)
                else:
                    path.setDelay(self.getDelayForPath(path.route))
                    self.routesDelays.append(path)

    def decreaseBandwidth(self, traffic):
        for ingress in traffic:
            for demand in ingress.traffic:
                path = self.routes[(ingress.router, self.hosts[demand])]
                self.decreaseTrafficForPath(path.route, ingress.traffic[demand])

    def pathCreatesCongestion(self, path):
        for i in range(len(path) - 1):
            if self.topology[path[i]][path[i + 1]]['bandwidth'] < 0:
                return True
        return False

    def decreaseTrafficForPath(self, path, traffic):
        for i in range(len(path) - 1):
            self.topology[path[i]][path[i + 1]]['bandwidth'] -= traffic

    def getDelayForPath(self, path):
        delay = 0.0
        for i in range(len(path) - 1):
            delay += self.topology[path[i]][path[i + 1]]['latency']
        return delay

    def getRoutesDelay(self):
        return self.routesDelays

    def restoreLinksCapacity(self, originalTopology):
        self.topology = originalTopology.copy()

    def deleteOldDelays(self):
        self.routesDelays.clear()

    def reduceLinksHelper(self, x, currEl, noLinksModified, levelLinksToModify, lower_perc, upper_perc):
        inEl = currEl
        while currEl < inEl + int(levelLinksToModify * noLinksModified / 100):
            rand_boundaries = random.randrange(lower_perc, upper_perc)
            new_val_band = self.topology[x[currEl][0]][x[currEl][1]]['bandwidth'] - rand_boundaries * \
                           self.topology[x[currEl][0]][x[currEl][1]]['bandwidth'] / 100
            log_file.write("Link {}->{} has been reduced from {} to {} ({} %) \n".format(x[currEl][0], x[currEl][1],
                                                                                         self.topology[x[currEl][0]]
                                                                                         [x[currEl][1]]['bandwidth'],
                                                                                         new_val_band, rand_boundaries))
            self.topology[x[currEl][0]][x[currEl][1]]['bandwidth'] = new_val_band
            currEl += 1
        return currEl

    def reduceLinksCapacity(self):
        noEdges = len(self.topology.edges)
        noLinksModified = int(TOTAL_PERC_LINKS_TO_MODIFY * noEdges / 100)
        x = random.sample(self.topology.edges, noLinksModified)

        currEl = 0
        currEl = self.reduceLinksHelper(x, currEl, noLinksModified, FIRST_PERC_LINKS_TO_MODIFY, LOWER_FIRST_PERC,
                                        UPPER_FIRST_PERC)
        currEl = self.reduceLinksHelper(x, currEl, noLinksModified, SECOND_PERC_LINKS_TO_MODIFY, LOWER_SECOND_PERC,
                                        UPPER_SECOND_PERC)
        currEl = self.reduceLinksHelper(x, currEl, noLinksModified, THIRD_PERC_LINKS_TO_MODIFY, LOWER_THIRD_PERC,
                                        UPPER_THIRD_PERC)
        self.reduceLinksHelper(x, currEl, noLinksModified, FOURTH_PERC_LINKS_TO_MODIFY, LOWER_FOURTH_PERC,
                               UPPER_FOURTH_PERS)
