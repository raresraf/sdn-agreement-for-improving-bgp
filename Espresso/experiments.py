from Espresso.cp import CP
from Espresso import espresso
from Espresso import randomizers
from Espresso import setupTopology
from Espresso.cp import CP
from Espresso.isp import ISP
from Espresso.isp import setLogFile as ispSetLogFile
from Espresso.plots import plotTrafficSentPerRound, plotCDFPerRounds
from Espresso.setupTopology import startOptimization
from Espresso.topology import Topology

all_topologies = ["_Rocketfuel_rf1221", "_Rocketfuel_rf1239", "_Rocketfuel_rf1755", "_Rocketfuel_rf3257",
                  "_Rocketfuel_rf3967", "_Rocketfuel_rf6461"]

topology_name = "_topReport"
log_file = "results/log" + topology_name + ".txt"

PERC_PATHS_TO_MOVE_TRAFFIC = 1/5
PERC_TRAFFIC_TO_MOVE = 10/100

NO_TOPOLOGIES_EXAMINED = 40


def buildTopology(cp, isp, noEgressRoutersHosts=None, noIngressRoutersISP=None, maxCapacity=0):
    topology = Topology()
    topology.buildTopology(cp, isp, noEgressRoutersHosts=noEgressRoutersHosts, noIngressRoutersISP=noIngressRoutersISP,
                           maxCapacity=maxCapacity)
    return topology


def getResultsTrafficRandomizedHalfCap():
    aggregated_results_traffic_half_maxcap = []
    for i in range(0, NO_TOPOLOGIES_EXAMINED):
        cp = CP("../configurations/configSender" + topology_name + ".txt")
        isp = ISP("../configurations/configReceiver" + topology_name + ".txt")
        topology = Topology()
        topology.buildTopology(cp, isp, maxCapacity=1/2)
        aggregated_results_traffic_half_maxcap.append(startOptimization(topology, cp, isp))
    plotCDFPerRounds(aggregated_results_traffic_half_maxcap, xlabel='Bandwidth in Gbps', ylabel='CDF', title='CDF Randomized Traffic Sent Per Rounds',
                     path_save_figure="results/CDFTrafficRandomizedAcrossRounds" + topology_name + ".png")


def getResultsTrafficRandomized():
    aggregated_results_traffic_half_maxcap = []
    aggregated_results_traffic_maxcap = []
    aggregated_results_traffic_twice_maxcap = []

    for i in range(0, NO_TOPOLOGIES_EXAMINED):
        cp = CP("../configurations/configSender" + topology_name + ".txt")
        isp = ISP("../configurations/configReceiver" + topology_name + ".txt")
        topology = Topology()
        topology.buildTopology(cp, isp, maxCapacity=1/2)
        aggregated_results_traffic_half_maxcap.append(startOptimization(topology, cp, isp))
        topology.buildTopology(cp, isp, maxCapacity=1)
        aggregated_results_traffic_maxcap.append(startOptimization(topology, cp, isp))
        topology.buildTopology(cp, isp, maxCapacity=2)
        aggregated_results_traffic_twice_maxcap.append(startOptimization(topology, cp, isp))

    return [aggregated_results_traffic_half_maxcap, aggregated_results_traffic_maxcap,
            aggregated_results_traffic_twice_maxcap]


def plotSimpleTopologyResults():
    global log_file
    log_file = open(log_file, "w")
    setupTopology.setLogFile(log_file)
    espresso.setLogFile(log_file)
    randomizers.setLogFile(log_file)
    ispSetLogFile(log_file)
    filename_cp = "../configurations/configSender" + topology_name + ".txt"
    filename_isp = "../configurations/configReceiver" + topology_name + ".txt"
    cp = CP(filename_cp)
    isp = ISP(filename_isp)
    topology = buildTopology(cp, isp)
    delays = startOptimization(topology, cp, isp)
    plotTrafficSentPerRound(delays, topology_name)
    # plotResultsTraffic()
    # getResultsTrafficRandomizedHalfCap()


def plotResultsPerRoundsAllTop():
    global log_file
    global topology_name
    all_delays = []
    for top_name in all_topologies:
        topology_name = top_name
        log_file = "results/log" + topology_name + ".txt"
        log_f = open(log_file, "w")
        setupTopology.setLogFile(log_f)
        espresso.setLogFile(log_f)
        randomizers.setLogFile(log_f)
        ispSetLogFile(log_f)
        filename_cp = "../configurations/configSender" + topology_name + ".txt"
        filename_isp = "../configurations/configReceiver" + topology_name + ".txt"
        cp = CP(filename_cp)
        isp = ISP(filename_isp)
        topology = buildTopology(cp, isp, noEgressRoutersHosts=10, noIngressRoutersISP=10, maxCapacity=1)
        delays = startOptimization(topology, cp, isp)
        for round in range(0, len(delays)):
            if len(all_delays) == 0:
                for round in range(0,len(delays)):
                    all_delays.append([])
            for delay in delays[round]:
                all_delays[round].append(delay)
        plotCDFPerRounds(delays, 'Rounds', '% of Traffic Encountering Congestion',
                         'CDF Randomized Traffic Sent per Rounds',
                         "results/Espresso" + topology_name + ".png")
    plotCDFPerRounds(all_delays, '% of Traffic Encountering Congestion', 'Rounds',
                     'CDF Randomized Traffic Sent per Rounds',
                     "results/EspressoResultsPerRoundAllASes.png")


class Experiments:
    if __name__ == "__main__":
        plotSimpleTopologyResults()
        # plotResultsPerRoundsAllTop()