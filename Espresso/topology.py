import networkx as nx

from Espresso.randomizers import randomizeIngressRouters, randomizeEgressRouters, randomizeTraffic


class Topology(nx.Graph):

    def __init__(self):
        super().__init__()

    def buildTopology(self, cp, isp, noEgressRoutersHosts=None, noIngressRoutersISP=None, maxCapacity=0):
        for node in cp.egress_points:
            if node not in self.nodes:
                self.add_node(node)
        for node in isp.routers:
            if node not in self.nodes:
                self.add_node(node)
        for link in isp.links:
            self.add_edge(link.router1, link.router2, bandwidth=link.capacity, latency=link.latency)
            self.add_edge(link.router2, link.router1, bandwidth=link.capacity, latency=link.latency)
        if noIngressRoutersISP is not None:
            randomizeIngressRouters(self, cp, isp, noIngressRoutersISP)
        if noEgressRoutersHosts is not None:
            randomizeEgressRouters(self, cp, isp, noEgressRoutersHosts)
        if maxCapacity is not 0:
            randomizeTraffic(self, cp, maxCapacity)