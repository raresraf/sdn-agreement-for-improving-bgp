import math
import numpy as np
import statistics
from matplotlib import pyplot as plt


def extractTrafficSent(routes):
    traffic = 0
    for route in routes:
        if route.delay != math.inf:
            traffic += route.traffic
    return traffic


def plotResultsAllRounds(results, topology_name):
    minTraffic = []
    minDelays = []
    medianTraffic = []
    medianDelays = []
    maxTraffic = []
    maxDelays = []
    maxVal = 0

    for result in results:
        minDelays.append(-min(route.delay for route in result))
        minTraffic.append(min(route.traffic for route in result)*5)
        medianDelays.append(-statistics.median(route.delay for route in result))
        medianTraffic.append(statistics.median(route.traffic for route in result)*5)
        maxDelays.append(-max(route.delay for route in result))
        maxTraffic.append(max(route.traffic for route in result)*5)
        maxValueFound = max((route.delay for route in result if route.delay != math.inf), default=0)
        if maxValueFound != math.inf:
            maxVal = max(maxVal, maxValueFound)

    maxVal = -maxVal-10

    arrays = [minDelays, medianDelays, maxDelays]
    for array in arrays:
        for i, result in enumerate(array):
            if result == -math.inf:
                array[i] = maxVal

    fig = plt.figure(num=1, figsize=(20, 6), dpi=80, facecolor='w', edgecolor='k')
    ax = fig.add_subplot(111)

    ax.axhline(y=maxVal, color='gray', linestyle='--', label="Infinity")

    color_list = ['lavender', 'violet', 'plum', 'SlateBlue', 'magenta', 'purple']

    gap = .8 / len([minDelays])
    for i, row in enumerate([minDelays]):
        X = np.arange(len(row))
        ax.bar(X * gap, row, width=gap, color=color_list[0], edgecolor='black', label="Min. Delay/traffic")

    gap = .8 / len([minTraffic])
    for i, row in enumerate([minTraffic]):
        X = np.arange(len(row))
        ax.bar(X * gap, row, width=gap, color=color_list[0], edgecolor='black')

    last_gap = (len(minDelays) + 1) * gap

    gap = .8 / len([medianDelays])
    for i, row in enumerate([medianDelays]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[1], edgecolor='black', label="Median Delay/Traffic")

    gap = .8 / len([medianTraffic])
    for i, row in enumerate([medianTraffic]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[1], edgecolor='black')

    last_gap = last_gap + (len(medianDelays) + 1) * gap

    gap = .8 / len([maxDelays])
    for i, row in enumerate([maxDelays]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[3], edgecolor='black', label="Max. Delay/Traffic")

    gap = .8 / len([maxTraffic])
    for i, row in enumerate([maxTraffic]):
        X = np.arange(len(row))
        ax.bar(last_gap + X * gap, row, width=gap, color=color_list[3], edgecolor='black')

    ax.legend(loc=2, bbox_to_anchor=(0.01, -0.1), ncol=3)

    ax.axhline(0, color='black')
    plt.xlabel('Results per rounds')
    plt.ylabel('Latency in ms vs. Bandwidth in GB')
    plt.title('Results per Rounds')

    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.2)

    plt.savefig("results/resultsPerRounds" + topology_name + ".png")
    plt.show()


def plotTrafficSentPerRound(results, topology_name):
    all_delays = []
    all_traffic = []
    maxVal = 0
    xticks = []

    for result in results:
        round_traffic = []
        round_delays = []
        for route in result:
            if route.delay != math.inf:
                maxVal = max(maxVal, route.delay)
            round_delays.append(-route.delay)
            round_traffic.append(route.traffic)
            xticks.append("{} -> {}".format(route.source, route.target))
        all_delays.append(round_delays)
        all_traffic.append(round_traffic)

    if 0 <= maxVal < 10:
        maxVal = -maxVal - 8
    else:
            maxVal = -maxVal - 50

    for result in all_delays:
        for i, delay in enumerate(result):
            if delay == -math.inf:
                result[i] = maxVal

    fig = plt.figure(num=1, figsize=(20, 6), dpi=80, facecolor='w', edgecolor='k')
    ax = fig.add_subplot(111)

    ax.axhline(y=maxVal, color='gray', linestyle='--', label="Infinity")

    color_list = ['lavender', 'violet', 'plum', 'SlateBlue', 'magenta', 'purple', 'red', 'cyan', 'yellow', 'blue']
    last_gap = 0
    xticksPos = []

    for i, result in enumerate(all_delays):
        gap = .8 / len([result])
        for j, row in enumerate([result]):
            X = np.arange(len(row))
            ax.bar(last_gap + X * gap, row, width=gap, color=color_list[i%10], edgecolor='black')
        #xticksPos.append(last_gap + 2 * gap - gap/2)
        #xticks.append("Round {}". format(i+1))
        for j in X:
            xticksPos.append(last_gap + j * gap)

        for j, row in enumerate([all_traffic[i]]):
            X = np.arange(len(row))
            ax.bar(last_gap + X * gap, row, width=gap, color=color_list[i%10], edgecolor='black')

        last_gap = last_gap + (len(result) + 1) * gap

    ax.legend(loc=0)

    ax.axhline(0, color='black')
    locs = ax.get_yticks()
    labels = []
    for loc in locs:
        if loc < 0:
            labels.append(-loc)
        else:
            labels.append(loc)
    labels[0] = ' '
    if labels[1] > -maxVal:
        labels[1] = ' '
    plt.yticks(locs, labels)
    plt.xticks(xticksPos, xticks, rotation=90, fontsize=10)


    plt.xlabel('Results per rounds')
    plt.ylabel('Latency in ms vs. Bandwidth in Gbps')
    plt.title('Espresso Bandwidth vs. Delay Results per Round')

    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.2)

    plt.savefig("results/BandDelResultsPerRound" + topology_name + ".png")
    plt.show()


def extractPercCongestion(list_result):
    total_band_not_sent = 0
    total_traffic_to_be_sent = 0
    for route in list_result:
        if route.delay == math.inf:
            total_band_not_sent += route.traffic
        total_traffic_to_be_sent += route.traffic
    return total_band_not_sent/total_traffic_to_be_sent * 100


def plotCDFPerRounds(list_results, xlabel, ylabel, title, path_save_figure):
    results = []

    rounds = len(list_results)
    for i in range(0, rounds):
        trafficForRound = extractPercCongestion(list_results[i])
        results.append(trafficForRound)

    fig = plt.figure(num=1, figsize=(8, 8), facecolor='w', edgecolor='k')
    ax = fig.add_subplot(111)
    x = np.arange(0, rounds, 1)
    ax.plot(x, results)
    ax.fill_between(x, results)

    ax.legend(loc=2, bbox_to_anchor=(0.01, -0.1), ncol=3)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    #ax.set_title(title)
    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.32)
    plt.savefig(path_save_figure)
    plt.show()
