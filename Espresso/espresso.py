import math

from Espresso.egressRouter import EgressRouter

log_file = None


def raise_KeyError(msg='Host doesn\'t exist'): raise KeyError(msg)


def setLogFile(logFile):
    global log_file
    log_file = logFile


def findWorstDelayPath(alreadyUsedEgresses, delays):
    maxDelay = 0
    worstPath = None
    for result in delays:
        sourceEgressPairExists = [i[1] for i in alreadyUsedEgresses if i[0] == result.source]
        if (len(sourceEgressPairExists) == 0 or result.target not in sourceEgressPairExists) and result.delay > maxDelay:
            maxDelay = result.delay
            worstPath = result
    alreadyUsedEgresses.append((worstPath.source, worstPath.target))
    log_file.write("Worst delay traffic at router {} for host {} with delay {} \n".format(worstPath.source,
                                                                                          worstPath.target, maxDelay))
    return worstPath


def findBestDelayPathForPrefix(delays, initialRouter, prefix):
    alternativeEgresses = [router for router in delays if router.target == prefix and router.source != initialRouter]
    minDelay = math.inf
    bestEgress = None
    for result in alternativeEgresses:
        if result.delay <= minDelay:
            minDelay = result.delay
            bestEgress = result.source
    if minDelay == math.inf:
        return -1
    return bestEgress


def moveTraffic(trafficDemands, router, host, trafficToMove):
    egressMoveTo = [i for i in trafficDemands if i.router == router][0]
    try:
        egressMoveTo.traffic[host] += trafficToMove or raise_KeyError('Host doesn\t exist')
    except KeyError:
        print('Host does not exist in dictionary')


def removeTraffic(trafficDemands, router, host, trafficToRemove):
    egressMoveTo = [i for i in trafficDemands if i.router == router][0]
    try:
        egressMoveTo.traffic[host] -= trafficToRemove or raise_KeyError('Host doesn\t exist')
    except KeyError:
        print('Host does not exist in dictionary')


class Espresso:
    def __init__(self, topology, cp):
        self.cp = cp
        self.noEgressPoints = len(cp.egress_points)
        self.topology = topology
        self.egressRouters = []

    # On start, send even amount of traffic to all egress points
    def start(self):
        for egress in self.cp.egress_points:
            newEgressRouter = EgressRouter(egress)
            for demand in self.cp.traffic:
                newEgressRouter.traffic[demand] = self.cp.traffic[demand]/self.noEgressPoints
            self.egressRouters.append(newEgressRouter)

    def getTrafficDemands(self):
        return self.egressRouters

