class Route:
    def __init__(self, source, target, route):
        self.source = source
        self.target = target
        self.route = route
        self.delay = None
        self.traffic = None

    def setDelay(self, delay):
        self.delay = delay

    def setTraffic(self, traffic):
        self.traffic = traffic
