You can find the Rocketfuel topologies in the configurations file.

In order to run the experiments for the Negotiation Protocol:
	1. Go to NegotiationProtocol
	2. Go to configurationRun.py
	3. Comment/ Uncomment the 4 functions from __main__:
		a. plotSimpleResults(): plots the exchange of flows between the CP and ISP
		b. plotSimpleResultsLargeTopology(): same as a), but can randomize ingress routers/egress routers/traffic
		c. plotResultsOneTopology(): plots all of the CDFs for the specified topology, with a comparison between 
			latencies and traffic sent for the six strategies and results for different values of ingress routers/egress routers/traffic
		d. plotResultsAllTopologies(): same as c), but aggregated all of the results on all topologies
	4. Run configurationRun.py
	5. You can change the topology that is being run by changing the global variable topology_name in configurationRun.py

To run Espresso experiments:
	1. Go to Espresso
	2. Go to experiments.py
	3. Comment/ Uncomment the 2 functions from __main__:
		a. plotSimpleTopologyResults(): plots the exchange of flows between the CP and ISP for the specified number of rounds
		b. plotSimpleResultsLargeTopology(): plots CDFs for amount of traffic that creates congestion for all rounds for
			a specific number of topologies
	4. Run experiments.py
	5. You can change the topology that is being run by changing the global variable topology_name in experiments.py
	6. You can change the number of topologies randomized by changing the global variable NO_TOPOLOGIES_EXAMINED in experiments.py
	7. You can change the number of rounds by changing the global variable NO_ROUNDS in setupTopology.py
